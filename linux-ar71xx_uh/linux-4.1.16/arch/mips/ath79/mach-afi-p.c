/*
 *  Derived from mach-ubnt-xm.c
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/pci.h>
#include <linux/platform_device.h>
#include <linux/ath9k_platform.h>
#include <linux/etherdevice.h>
#include <linux/gpio.h>
#include <linux/timer.h>
#include <linux/platform_data/phy-at803x.h>

#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/irq.h>
#include <asm/mach-ath79/ar71xx_regs.h>

#include "common.h"
#include "dev-ap9x-pci.h"
#include "dev-eth.h"
#include "dev-gpio-buttons.h"
#include "dev-leds-gpio.h"
#include "dev-usb.h"
#include "dev-wmac.h"
#include "machtypes.h"

#include "afi-common.h"

#define AFIP_GPIO_PHY_RESET		0
#define AFIP_GPIO_WL_ACTIVE		2
#define AFIP_GPIO_BT_RESET		13

#define AFIP_GPIO_BUZZER    3
#define AFIP_GPIO_LED_CLK  14
#define AFIP_GPIO_LED_DATA 11

#define AFIP_WMAC_CALDATA_OFFSET	0x1000
#define AFIP_KEYS_POLL_INTERVAL		20
#define AFIP_KEYS_DEBOUNCE_INTERVAL	(3 * AFIP_KEYS_POLL_INTERVAL)

static struct gpio_keys_button afip_gpio_keys[] __initdata = {
	{
		.desc			= "reset",
		.type			= EV_KEY,
		.code			= KEY_RESTART,
		.debounce_interval	= AFIP_KEYS_DEBOUNCE_INTERVAL,
		.gpio			= 12,
		.active_low		= 1,
	}
};

static struct mdio_board_info afip_mdio0_info[] = {
	{
		.bus_id = "ag71xx-mdio.0",
		.phy_addr = 0,
		.platform_data = NULL,
	},
};

static struct timer_list gpio_bt_reset_timer;
static void gpio_bt_reset_timer_cb(unsigned long x)
{
	gpio_set_value(AFIP_GPIO_BT_RESET, 1);
	gpio_export(AFIP_GPIO_BT_RESET, true);
}

static void __init afip_setup(void)
{
	u8 mac[6] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
	afi_init_flash();

	gpio_request_one(AFIP_GPIO_PHY_RESET, GPIOF_OUT_INIT_HIGH, "PHY Reset");
	gpio_request_one(AFIP_GPIO_WL_ACTIVE, GPIOF_OUT_INIT_LOW, "Wl Active");

	/* Controlled by uh_leds */
	gpio_request_one(AFIP_GPIO_BUZZER, GPIOF_OUT_INIT_LOW, "Buzzer");
	gpio_request_one(AFIP_GPIO_LED_CLK, GPIOF_OUT_INIT_LOW, "LED Clk");
	gpio_request_one(AFIP_GPIO_LED_DATA, GPIOF_OUT_INIT_LOW, "LED Data");

	gpio_request_one(AFIP_GPIO_BT_RESET, GPIOF_OUT_INIT_LOW, "BT Reset");
	setup_timer(&gpio_bt_reset_timer, gpio_bt_reset_timer_cb, 0);
	mod_timer(&gpio_bt_reset_timer, jiffies + msecs_to_jiffies(50));

	ath79_register_gpio_keys_polled(-1, AFIP_KEYS_POLL_INTERVAL,
                                        ARRAY_SIZE(afip_gpio_keys),
                                        afip_gpio_keys);

	ath79_register_usb();
	ath79_setup_ar934x_eth_cfg(AR934X_ETH_CFG_MII_GMAC0);

	ath79_register_mdio(0, ~BIT(1));
	mdiobus_register_board_info(afip_mdio0_info,
				    ARRAY_SIZE(afip_mdio0_info));

	ath79_init_mac(ath79_eth0_data.mac_addr, afi_eeprom + AFIP_WMAC_CALDATA_OFFSET + 2, 0);
	ath79_eth0_data.phy_if_mode = PHY_INTERFACE_MODE_MII;
	ath79_eth0_data.phy_mask = BIT(1);
	ath79_eth0_data.speed = 100;
	ath79_eth0_data.duplex = DUPLEX_FULL;
	ath79_register_eth(0);

	ath79_init_mac(mac, afi_eeprom, 0);
	ath79_register_wmac(afi_eeprom + AFIP_WMAC_CALDATA_OFFSET, mac);
}

MIPS_MACHINE(ATH79_MACH_AFI_MESHPOINT, "AFi-P", "Ubiquiti AmpliFi Mesh Point",
	     afip_setup);
