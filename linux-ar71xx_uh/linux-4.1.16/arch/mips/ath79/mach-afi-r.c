#include <linux/platform_device.h>
#include <linux/ath9k_platform.h>
#include <linux/ar8216_platform.h>
#include <linux/platform_data/phy-at803x.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/timer.h>
#include <linux/i2c-gpio.h>
#include <linux/i2c.h>
#include <linux/cyttsp5_core.h>
#include <linux/cyttsp5_platform.h>

#include <asm/mach-ath79/ar71xx_regs.h>
#include <asm/mach-ath79/irq.h>

#include "common.h"
#include "machtypes.h"
#include "pci.h"
#include "dev-eth.h"
#include "dev-gpio-buttons.h"
#include "dev-leds-gpio.h"
#include "dev-usb.h"
#include "dev-wmac.h"
#include "dev-ap9x-pci.h"

#include "afi-common.h"

#define AFIR_GPIO_LED_BOT_1		5
#define AFIR_GPIO_LED_BOT_2		6
#define AFIR_GPIO_LED_BOT_3		7

#define AFIR_GPIO_QUICK_CHARGE		17
#define AFIR_GPIO_WL_ACTIVE		19
#define AFIR_GPIO_BT_RESET		8

#define AFIR_GPIO_BTN_RESET		2
#define AFIR_KEYS_POLL_INTERVAL		20 /* msecs */
#define AFIR_KEYS_DEBOUNCE_INTERVAL	(3 * AFIR_KEYS_POLL_INTERVAL)

#define AFIR_WMAC_MAC_OFFSET		2
#define AFIR_PCIE_MAC_OFFSET		6
#define AFIR_WMAC_CALDATA_OFFSET	0x1000
#define AFIR_PCIE_CALDATA_OFFSET	0x5000

#define AFIR_GPIO_MDC			3
#define AFIR_GPIO_MDIO			4

#define AFIRX_GPIO_I2C_SCL		15
#define AFIRX_GPIO_I2C_SDA		19

#define CYTTSP5_I2C_ADDRESS		0x24
#define CYTTSP5_I2C_IRQ_GPIO		14
#define CYTTSP5_I2C_RST_GPIO 		21

#define CYTTSP5_HID_DESC_REGISTER 1

#define CY_VKEYS_X		720
#define CY_VKEYS_Y		1280
#define CY_MAXX			880
#define CY_MAXY			1280
#define CY_MINX			0
#define CY_MINY			0

#define CY_ABS_MIN_X		CY_MINX
#define CY_ABS_MIN_Y		CY_MINY
#define CY_ABS_MAX_X		CY_MAXX
#define CY_ABS_MAX_Y		CY_MAXY
#define CY_ABS_MIN_P		0
#define CY_ABS_MAX_P		255
#define CY_ABS_MIN_W		0
#define CY_ABS_MAX_W		255
#define CY_ABS_MIN_T 		0
#define CY_ABS_MAX_T 		15

static struct gpio_keys_button afir_gpio_keys[] __initdata = {
	{
		.desc		= "Reset button",
		.type		= EV_KEY,
		.code		= KEY_RESTART,
		.debounce_interval = AFIR_KEYS_DEBOUNCE_INTERVAL,
		.gpio		= AFIR_GPIO_BTN_RESET,
		.active_low	= 1,
	},
};

static struct ar8327_pad_cfg afir_ar8327_pad0_cfg = {
	.mode = AR8327_PAD_MAC_SGMII,
	.sgmii_delay_en = true,
};

static struct ar8327_platform_data afir_ar8327_data = {
	.pad0_cfg = &afir_ar8327_pad0_cfg,
	.port0_cfg = {
		.force_link = 1,
		.speed = AR8327_PORT_SPEED_1000,
		.duplex = 1,
		.txpause = 0,
		.rxpause = 0,
	},
};

static struct mdio_board_info afir_mdio0_info[] = {
	{
		.bus_id = "ag71xx-mdio.0",
		.phy_addr = 0,
		.platform_data = &afir_ar8327_data,
	},
};

static struct cyttsp5_core_platform_data _cyttsp5_core_platform_data = {
	.irq_gpio = CYTTSP5_I2C_IRQ_GPIO,
	.rst_gpio = CYTTSP5_I2C_RST_GPIO,
	.hid_desc_register = CYTTSP5_HID_DESC_REGISTER,
	.xres = NULL, // cyttsp5_xres, // Use software reset instead (gpio shared with LCD reset)
	.init = cyttsp5_init,
	.power = cyttsp5_power,
	.detect = cyttsp5_detect,
	.irq_stat = cyttsp5_irq_stat,
	.flags = CY_CORE_FLAG_RESTORE_PARAMETERS,
	.easy_wakeup_gesture = CY_CORE_EWG_NONE,
};

static const int16_t cyttsp5_abs[] = {
	ABS_MT_POSITION_X, CY_ABS_MIN_X, CY_ABS_MAX_X, 0, 0,
	ABS_MT_POSITION_Y, CY_ABS_MIN_Y, CY_ABS_MAX_Y, 0, 0,
	ABS_MT_PRESSURE, CY_ABS_MIN_P, CY_ABS_MAX_P, 0, 0,
	CY_IGNORE_VALUE, CY_ABS_MIN_W, CY_ABS_MAX_W, 0, 0,
	ABS_MT_TRACKING_ID, CY_ABS_MIN_T, CY_ABS_MAX_T, 0, 0,
	ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0,
	ABS_MT_TOUCH_MINOR, 0, 255, 0, 0,
	ABS_MT_ORIENTATION, -127, 127, 0, 0,
	ABS_MT_TOOL_TYPE, 0, MT_TOOL_MAX, 0, 0,
	ABS_MT_DISTANCE, 0, 255, 0, 0,	/* Used with hover */
};

struct touch_framework cyttsp5_framework = {
	.abs = (uint16_t *)&cyttsp5_abs[0],
	.size = ARRAY_SIZE(cyttsp5_abs),
	.enable_vkeys = 0,
};

static struct cyttsp5_mt_platform_data _cyttsp5_mt_platform_data = {
	.frmwrk = &cyttsp5_framework,
	.flags = CY_MT_FLAG_INV_X,
	.inp_dev_name = CYTTSP5_MT_NAME,
	.vkeys_x = CY_VKEYS_X,
	.vkeys_y = CY_VKEYS_Y,
};

static struct cyttsp5_platform_data _cyttsp5_platform_data = {
	.core_pdata = &_cyttsp5_core_platform_data,
	.mt_pdata = &_cyttsp5_mt_platform_data,
	.loader_pdata = &_cyttsp5_loader_platform_data,
};

static ssize_t cyttsp5_virtualkeys_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf,
		__stringify(EV_KEY) ":"
		__stringify(KEY_BACK) ":1360:90:160:180:"
		__stringify(EV_KEY) ":"
		__stringify(KEY_MENU) ":1360:270:160:180:"
		__stringify(EV_KEY) ":"
		__stringify(KEY_HOMEPAGE) ":1360:450:160:180:"
		__stringify(EV_KEY) ":"
		__stringify(KEY_SEARCH) ":1360:630:160:180\n");
}

static struct kobj_attribute cyttsp5_virtualkeys_attr = {
	.attr = {
		.name = "virtualkeys.cyttsp5_mt",
		.mode = S_IRUGO,
	},
	.show = &cyttsp5_virtualkeys_show,
};

static struct attribute *cyttsp5_properties_attrs[] = {
	&cyttsp5_virtualkeys_attr.attr,
	NULL
};

static struct attribute_group cyttsp5_properties_attr_group = {
	.attrs = cyttsp5_properties_attrs,
};

static struct i2c_board_info afirx_i2c_board_info[] = {
  	{
		I2C_BOARD_INFO(CYTTSP5_I2C_NAME, CYTTSP5_I2C_ADDRESS),
		.irq =  ATH79_GPIO_IRQ(CYTTSP5_I2C_IRQ_GPIO),
		.platform_data = &_cyttsp5_platform_data,
	},
};

static struct i2c_gpio_platform_data afirx_i2c_gpio_data = {
	.sda_pin = AFIRX_GPIO_I2C_SDA,
	.scl_pin = AFIRX_GPIO_I2C_SCL,
	.timeout = 500,
};

static struct platform_device afirx_i2c_gpio_device = {
	.name = "i2c-gpio",
	.id = 0,
	.dev = {
		.platform_data = &afirx_i2c_gpio_data,
	},
};

static void __init afir_mdio_setup(void)
{
	void __iomem *base;
	u32 t;

	base = ioremap(AR71XX_GPIO_BASE, AR71XX_GPIO_SIZE);
	//output enable on 3 and 4
	 __raw_writel(__raw_readl(base + AR71XX_GPIO_REG_OE) & ~(1 << AFIR_GPIO_MDC), base + AR71XX_GPIO_REG_OE);
	 __raw_writel(__raw_readl(base + AR71XX_GPIO_REG_OE) & ~(1 << AFIR_GPIO_MDIO), base + AR71XX_GPIO_REG_OE);
	//setup mux
	ath79_gpio_output_select(AFIR_GPIO_MDC, QCA956X_GPIO_OUT_MUX_GE0_MDC);
	ath79_gpio_output_select(AFIR_GPIO_MDIO, QCA956X_GPIO_OUT_MUX_GE0_MDO);
#define GPIO_IN_ENABLE3_ADDRESS			0x0050
#define GPIO_IN_ENABLE3_MII_GE0_MDI		0x00ff0000
#define GPIO_IN_ENABLE3_MII_GE0_MDI_SHIFT	16
#define GPIO_IN_ENABLE3_MII_GE0_MDI_SET(x)	(((x) << GPIO_IN_ENABLE3_MII_GE0_MDI_SHIFT) & GPIO_IN_ENABLE3_MII_GE0_MDI)
	t = __raw_readl(base + GPIO_IN_ENABLE3_ADDRESS);
	t &= ~GPIO_IN_ENABLE3_MII_GE0_MDI;
	t |= GPIO_IN_ENABLE3_MII_GE0_MDI_SET(AFIR_GPIO_MDIO);
	__raw_writel(t, base + GPIO_IN_ENABLE3_ADDRESS);
	iounmap(base);
}

static void __init afir_print_reg(const char *name, uint32_t reg)
{
	void __iomem *base;
	u32 v;

	base = ioremap(reg, 4);
	v = __raw_readl(base);
	printk("%s: 0x%08x\n", name, v);

	iounmap(base);
}

static void __init afir_gmac_setup(void)
{
	void __iomem *base;
	u32 v;

	/* make sure gmac is set to sgmii mode, seems to be chip default */
	ath79_setup_qca956x_eth_cfg(QCA956X_ETH_CFG_GE0_SGMII);

	base = ioremap(0x18070034, 4); // SGMII_CONFIG
	v = __raw_readl(base);
	v = (1<<10)		// MDIO_ENABLE
		| (2<<6)	// SPEED=1000
		| (1<<5)	// FORCE_SPEED
		| (1<<3)	// ENABLE_SGMII_TX_PAUSE (when in SGMII_PHY mode)
		| (2);		// 2=SGMII_MAC (1=SGMII_PHY)
	__raw_writel(v, base);
	iounmap(base);
}

static struct timer_list gpio_bt_reset_timer;
static void gpio_bt_reset_timer_cb(unsigned long x)
{
	gpio_set_value(AFIR_GPIO_BT_RESET, 1);
	gpio_export(AFIR_GPIO_BT_RESET, true);
}

static int __init cyttsp5_init_device(void)
{
	struct kobject *properties_kobj;
	int ret = 0;

	properties_kobj = kobject_create_and_add("board_properties", NULL);
	if (properties_kobj) {
		ret = sysfs_create_group(properties_kobj,
				&cyttsp5_properties_attr_group);
	}
	if (!properties_kobj || ret) {
		pr_err("%s: failed to create board_properties\n", __func__);
	}

	return ret;
}

static void __init afir_setup(void)
{
	u8 mac[6] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
	afi_init_flash();

	/* Enable Quick Charge */
	gpio_request_one(AFIR_GPIO_QUICK_CHARGE, GPIOF_OUT_INIT_HIGH, "Quick Charge");
	ath79_gpio_function_enable(2); /* Disable JTAG */
	gpio_export(AFIR_GPIO_QUICK_CHARGE, true);
	printk("Quick Charge enabled\n");

	if (is_afi_rx == false) {
		gpio_request_one(AFIR_GPIO_WL_ACTIVE, GPIOF_OUT_INIT_LOW, "Wl Active");
	}

	/* Controlled by uh_leds */
	gpio_request_one(AFIR_GPIO_LED_BOT_1, GPIOF_OUT_INIT_HIGH, "BOT LED 1");
	gpio_request_one(AFIR_GPIO_LED_BOT_2, GPIOF_OUT_INIT_LOW,  "BOT LED 2");
	gpio_request_one(AFIR_GPIO_LED_BOT_3, GPIOF_OUT_INIT_LOW,  "BOT LED 3");

	gpio_request_one(AFIR_GPIO_BT_RESET, GPIOF_OUT_INIT_LOW, "BT Reset");
	setup_timer(&gpio_bt_reset_timer, gpio_bt_reset_timer_cb, 0);
	mod_timer(&gpio_bt_reset_timer, jiffies + msecs_to_jiffies(50));

	ath79_register_gpio_keys_polled(-1, AFIR_KEYS_POLL_INTERVAL,
	                                ARRAY_SIZE(afir_gpio_keys),
	                                afir_gpio_keys);
	ath79_register_usb();

	afir_mdio_setup();
	ath79_register_mdio(0, 0x0);
	mdiobus_register_board_info(afir_mdio0_info,
				    ARRAY_SIZE(afir_mdio0_info));

	ath79_init_mac(mac, afi_eeprom + AFIR_WMAC_CALDATA_OFFSET + AFIR_WMAC_MAC_OFFSET, 0);
	ath79_register_wmac(afi_eeprom + AFIR_WMAC_CALDATA_OFFSET, mac);

	ath79_init_mac(mac, afi_eeprom + AFIR_PCIE_CALDATA_OFFSET + AFIR_PCIE_MAC_OFFSET, 0);
	ath79_register_pci();

	/* GMAC0 is connected to AR8327 switch in SGMII mode */
	afir_gmac_setup();
	ath79_init_mac(ath79_eth0_data.mac_addr, afi_eeprom, 0);
	ath79_eth0_data.phy_if_mode = PHY_INTERFACE_MODE_SGMII;
	ath79_eth0_data.phy_mask = BIT(0);
	ath79_eth0_data.mii_bus_dev = &ath79_mdio0_device.dev;
	ath79_register_eth(0);
	afir_print_reg("SGMII_CONFIG", 0x18070034);
	afir_print_reg("MR_AN_CONTROL", 0x1807001C);
	afir_print_reg("PLL (ETH_SGMII)", 0x18050048);

	if (is_afi_rx) {
		printk("Configuring I2C\n");
		i2c_register_board_info(0, afirx_i2c_board_info, ARRAY_SIZE(afirx_i2c_board_info));
		platform_device_register(&afirx_i2c_gpio_device);
		cyttsp5_init_device();
	}
}

MIPS_MACHINE(ATH79_MACH_AFI_ROUTER, "AFi-R", "Ubiquiti AmpliFi Router",
	     afir_setup);
