#include <linux/platform_device.h>
#include <linux/ath9k_platform.h>
#include <linux/ar8216_platform.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/timer.h>

#include <asm/mach-ath79/ar71xx_regs.h>

#include "common.h"
#include "machtypes.h"
#include "pci.h"
#include "dev-eth.h"
#include "dev-gpio-buttons.h"
#include "dev-leds-gpio.h"
#include "dev-usb.h"
#include "dev-wmac.h"

#include "afi-common.h"

#define AFIP_HD_GPIO_WL_ACTIVE		19
#define AFIP_HD_GPIO_BT_RESET		8

#define AFIP_HD_GPIO_BTN_RESET		2
#define AFIP_HD_KEYS_POLL_INTERVAL		20 /* msecs */
#define AFIP_HD_KEYS_DEBOUNCE_INTERVAL	(3 * AFIP_HD_KEYS_POLL_INTERVAL)

#define AFIP_HD_WMAC_MAC_OFFSET		2
#define AFIP_HD_PCIE_MAC_OFFSET		6
#define AFIP_HD_WMAC_CALDATA_OFFSET		0x1000
#define AFIP_HD_PCIE_CALDATA_OFFSET		0x5000

#define AFIP_HD_GPIO_MDC			3
#define AFIP_HD_GPIO_MDIO			4

#define AFIP_HD_GPIO_LED_CLK  6
#define AFIP_HD_GPIO_LED_DATA 5

static struct gpio_keys_button afip_hd_gpio_keys[] __initdata = {
	{
		.desc		= "Reset button",
		.type		= EV_KEY,
		.code		= KEY_RESTART,
		.debounce_interval = AFIP_HD_KEYS_DEBOUNCE_INTERVAL,
		.gpio		= AFIP_HD_GPIO_BTN_RESET,
		.active_low	= 1,
	},
};

static struct ar8327_pad_cfg afip_hd_ar8327_pad0_cfg = {
	.mode = AR8327_PAD_MAC_SGMII,
	.sgmii_delay_en = true,
};

static struct ar8327_platform_data afip_hd_qca8334_data = {
	.pad0_cfg = &afip_hd_ar8327_pad0_cfg,
	.port0_cfg = {
		.force_link = 1,
		.speed = AR8327_PORT_SPEED_1000,
		.duplex = 1,
		.txpause = 0,
		.rxpause = 0,
	},
};

static struct mdio_board_info afip_hd_mdio0_info[] = {
	{
		.bus_id = "ag71xx-mdio.0",
		.phy_addr = 0,
		.platform_data = &afip_hd_qca8334_data,
	},
};

static void __init afip_hd_mdio_setup(void)
{
	void __iomem *base;
	u32 t;

	base = ioremap(AR71XX_GPIO_BASE, AR71XX_GPIO_SIZE);
	//output enable on 3 and 4
	 __raw_writel(__raw_readl(base + AR71XX_GPIO_REG_OE) & ~(1 << AFIP_HD_GPIO_MDC), base + AR71XX_GPIO_REG_OE);
	 __raw_writel(__raw_readl(base + AR71XX_GPIO_REG_OE) & ~(1 << AFIP_HD_GPIO_MDIO), base + AR71XX_GPIO_REG_OE);
	//setup mux
	ath79_gpio_output_select(AFIP_HD_GPIO_MDC, QCA956X_GPIO_OUT_MUX_GE0_MDC);
	ath79_gpio_output_select(AFIP_HD_GPIO_MDIO, QCA956X_GPIO_OUT_MUX_GE0_MDO);
#define GPIO_IN_ENABLE3_ADDRESS			0x0050
#define GPIO_IN_ENABLE3_MII_GE0_MDI		0x00ff0000
#define GPIO_IN_ENABLE3_MII_GE0_MDI_SHIFT	16
#define GPIO_IN_ENABLE3_MII_GE0_MDI_SET(x)	(((x) << GPIO_IN_ENABLE3_MII_GE0_MDI_SHIFT) & GPIO_IN_ENABLE3_MII_GE0_MDI)
	t = __raw_readl(base + GPIO_IN_ENABLE3_ADDRESS);
	t &= ~GPIO_IN_ENABLE3_MII_GE0_MDI;
	t |= GPIO_IN_ENABLE3_MII_GE0_MDI_SET(AFIP_HD_GPIO_MDIO);
	__raw_writel(t, base + GPIO_IN_ENABLE3_ADDRESS);
	iounmap(base);
}

static void __init afip_hd_print_reg(const char *name, uint32_t reg)
{
	void __iomem *base;
	u32 v;

	base = ioremap(reg, 4);
	v = __raw_readl(base);
	printk("%s: 0x%08x\n", name, v);

	iounmap(base);
}

static void __init afip_hd_gmac_setup(void)
{
	void __iomem *base;
	u32 v;

	/* make sure gmac is set to sgmii mode, seems to be chip default */
	ath79_setup_qca956x_eth_cfg(QCA956X_ETH_CFG_GE0_SGMII);

	base = ioremap(0x18070034, 4); // SGMII_CONFIG
	v = __raw_readl(base);
	v = (1<<10)		// MDIO_ENABLE
		| (2<<6)	// SPEED=1000
		| (1<<5)	// FORCE_SPEED
		| (1<<3)	// ENABLE_SGMII_TX_PAUSE (when in SGMII_PHY mode)
		| (2);		// 2=SGMII_MAC (1=SGMII_PHY)
	__raw_writel(v, base);
	iounmap(base);
}

static struct timer_list gpio_bt_reset_timer;
static void gpio_bt_reset_timer_cb(unsigned long x)
{
	gpio_set_value(AFIP_HD_GPIO_BT_RESET, 1);
	gpio_export(AFIP_HD_GPIO_BT_RESET, true);
}

static void __init afip_hd_setup(void)
{
	u8 mac[6] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
	afi_init_flash();

	gpio_request_one(AFIP_HD_GPIO_WL_ACTIVE, GPIOF_OUT_INIT_LOW, "Wl Active");

	/* Controlled by uh_leds */
	gpio_request_one(AFIP_HD_GPIO_LED_CLK, GPIOF_OUT_INIT_LOW, "LED Clk");
	gpio_request_one(AFIP_HD_GPIO_LED_DATA, GPIOF_OUT_INIT_LOW, "LED Data");

	gpio_request_one(AFIP_HD_GPIO_BT_RESET, GPIOF_OUT_INIT_LOW, "BT Reset");
	setup_timer(&gpio_bt_reset_timer, gpio_bt_reset_timer_cb, 0);
	mod_timer(&gpio_bt_reset_timer, jiffies + msecs_to_jiffies(50));

	ath79_register_gpio_keys_polled(-1, AFIP_HD_KEYS_POLL_INTERVAL,
	                                ARRAY_SIZE(afip_hd_gpio_keys),
	                                afip_hd_gpio_keys);
	ath79_register_usb();

	afip_hd_mdio_setup();
	ath79_register_mdio(0, 0x0);
	mdiobus_register_board_info(afip_hd_mdio0_info,
				    ARRAY_SIZE(afip_hd_mdio0_info));

	ath79_init_mac(mac, afi_eeprom + AFIP_HD_WMAC_CALDATA_OFFSET + AFIP_HD_WMAC_MAC_OFFSET, 0);
	ath79_register_wmac(afi_eeprom + AFIP_HD_WMAC_CALDATA_OFFSET, mac);

	ath79_init_mac(mac, afi_eeprom + AFIP_HD_PCIE_CALDATA_OFFSET + AFIP_HD_PCIE_MAC_OFFSET, 0);
	ath79_register_pci();

	/* GMAC0 is connected to QCA8334 switch in SGMII mode */
	afip_hd_gmac_setup();
	ath79_init_mac(ath79_eth0_data.mac_addr, afi_eeprom, 0);
	ath79_eth0_data.phy_if_mode = PHY_INTERFACE_MODE_SGMII;
	ath79_eth0_data.phy_mask = BIT(0);
	ath79_eth0_data.mii_bus_dev = &ath79_mdio0_device.dev;
	ath79_register_eth(0);
	afip_hd_print_reg("SGMII_CONFIG", 0x18070034);
	afip_hd_print_reg("MR_AN_CONTROL", 0x1807001C);
	afip_hd_print_reg("PLL (ETH_SGMII)", 0x18050048);
}

MIPS_MACHINE(ATH79_MACH_AFI_MESHPOINT_HD, "AFi-P-HD", "Ubiquiti AmpliFi Mesh Point HD",
	     afip_hd_setup);
