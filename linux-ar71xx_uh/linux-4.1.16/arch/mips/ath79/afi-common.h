#ifndef AFI_COMMON_H
#define AFI_COMMON_H

#define ath_reg_rd(_phys) (*(volatile unsigned int *)KSEG1ADDR(_phys))
#define ath_reg_wr_nf(_phys, _val) \
	((*(volatile unsigned int *)KSEG1ADDR(_phys)) = (_val))

#define ath_reg_wr(_phys, _val) do { \
		ath_reg_wr_nf(_phys, _val); \
		ath_reg_rd(_phys); \
	} while(0)


#define ATH_SPI_FS		0x1f000000
#define ATH_SPI_WRITE		0x1f000008
#define ATH_SPI_RD_STATUS	0x1f00000c
#define ATH_SPI_CS_DIS		0x70000
#define ATH_SPI_CE_LOW		0x60000
#define ATH_SPI_CE_HIGH		0x60100
#define ATH_SPI_CE1_LOW		0x50000
#define ATH_SPI_CE1_HIGH	0x50100
#define ATH_SPI_CMD_WREAR	0xC5
#define ATH_SPI_CMD_WREN	0x06

#define ath_be_msb(_val, _i) (((_val) & (1 << (7 - _i))) >> (7 - _i))

#define ath_spi_bit_banger(_byte)  do { \
		int i; \
		for(i = 0; i < 8; i++) { \
			ath_reg_wr_nf(ATH_SPI_WRITE, \
			              ATH_SPI_CE_LOW | ath_be_msb(_byte, i)); \
			ath_reg_wr_nf(ATH_SPI_WRITE, \
			              ATH_SPI_CE_HIGH | ath_be_msb(_byte, i)); \
		} \
	} while(0)

#define ath_spi_go() do {	  \
		ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CE_LOW); \
		ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CS_DIS); \
	} while(0)

#define ath_spi_delay_8()    ath_spi_bit_banger(0)

#define ath_spi_poll() \
	do { \
		ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CS_DIS);\
		ath_spi_bit_banger(0x05); \
		ath_spi_delay_8(); \
		ath_spi_go(); \
	} while (ath_reg_rd(ATH_SPI_RD_STATUS) & 1)


#define AFI_EEPROM_SIZE (64*1024)

typedef enum
{
	LCD_COMMAND,
	LCD_DATA,
} afi_rx_lcd_cd_t;

void afi_init_flash(void);
void afi_reset_flash(void);
extern u8 *afi_eeprom;
extern bool is_afi_rx;

#endif
