/*
 *      upnp connection tracking helper
 */

#include <linux/module.h>
#include <linux/ip.h>
#include <net/route.h>
#include <linux/inetdevice.h>
#include <linux/skbuff.h>

#include <net/netfilter/nf_conntrack.h>
#include <net/netfilter/nf_conntrack_helper.h>
#include <net/netfilter/nf_conntrack_expect.h>

MODULE_AUTHOR("az <arturs.zoldners@ubnt.com>");
MODULE_DESCRIPTION("UPNP broadcast connection tracking helper");
MODULE_LICENSE("GPL");

#define UPNP_PORT  1900
// #define FILTER_SRC

#ifdef FILTER_SRC
#define IPMASK_MAX 16 /* max ip/mask tuples per dst.dev */
#endif

const char m_search[] = "M-SEARCH ";

int nf_conntrack_upnp_help(struct sk_buff *skb,
				unsigned int protoff,
				struct nf_conn *ct,
				enum ip_conntrack_info ctinfo,
				unsigned int timeout)
{
	struct nf_conntrack_expect *exp;
	struct nf_conntrack_tuple orig_tuple;
#ifdef FILTER_SRC
	struct rtable *rt = skb_rtable(skb);
	struct in_device *in_dev;
	int c;
	int i;
	__be32 ipmask[IPMASK_MAX][2];
#endif //FILTER_SRC
	char buff[sizeof(m_search) - 1];
	const char* ptr;

	if (skb->sk == NULL) //FIXME: local packets only?
		goto out;

	if (CTINFO2DIR(ctinfo) != IP_CT_DIR_ORIGINAL)
		goto out;

	orig_tuple = ct->tuplehash[IP_CT_DIR_ORIGINAL].tuple;

	if (orig_tuple.dst.u.udp.port != cpu_to_be16(UPNP_PORT))
		goto out;

	if ((__be32)orig_tuple.dst.u3.ip != (__be32)0xEFFFFFFA)  /* 239.255.255.250 */
		goto out;

	ptr = skb_header_pointer(skb, protoff + sizeof(struct udphdr), sizeof(buff), buff);
	if (!ptr) {
		printk(KERN_INFO "upnp: %s:(%d)\n", __FUNCTION__, __LINE__);
		goto out;
	}

	//look for 'M-SEARCH ' at the beginning of payload
	if (memcmp(ptr, m_search, sizeof(buff)) != 0) {
		goto out; /* not a search request */
	}

#ifdef FILTER_SRC
	{ //copy to local storage ip/mask values to release rcu lock ASAP
		c = 0;
		rcu_read_lock();
		in_dev = __in_dev_get_rcu(rt->dst.dev);
		if (in_dev != NULL) {
			for_ifa(in_dev) {
				ipmask[c][0] = ifa->ifa_local;
				ipmask[c][1] = ifa->ifa_mask;
				if(++c == IPMASK_MAX) break;
			} endfor_ifa(in_dev);
		}
		rcu_read_unlock();
	}

	if (c == 0)
		goto out;
#endif //FILTER_SRC

	exp = nf_ct_expect_alloc(ct);

	if (exp == NULL) {
		printk(KERN_INFO "upnp: %s:(%d)\n", __FUNCTION__, __LINE__);
		goto out;
	}

	exp->expectfn      = NULL;
	exp->flags         = NF_CT_EXPECT_PERMANENT;
	exp->class         = NF_CT_EXPECT_CLASS_DEFAULT;
	exp->helper        = NULL;

	//expected dst port is set from reversed tuple
	exp->tuple = ct->tuplehash[IP_CT_DIR_REPLY].tuple;

	//expected src port will be any
	exp->tuple.src.u.udp.port = 0;
	exp->mask .src.u.udp.port = 0;

#ifdef FILTER_SRC
	for (i = 0; i < c; ++i) {
		//take src IP/MASK from dst device's config
		exp->tuple.src.u3.ip      = ipmask[i][0] & ipmask[i][1];
		exp->mask .src.u3.ip      = ipmask[i][1];

		printk(
			KERN_INFO "upnp: expecting %pI4/%pI4 => %pI4:%d\n",
			&(exp->tuple.src.u3.ip),
			&(exp->mask .src.u3.ip),
			&(exp->tuple.dst.u3.ip),
			(int)(ntohs(exp->tuple.dst.u.udp.port))
		);

		nf_ct_expect_related(exp);
	}
#else //FILTER_SRC
	//set expected src IP to be any
	exp->tuple.src.u3.ip = 0;
	exp->mask .src.u3.ip = 0;
	/* printk(
		KERN_INFO "upnp: expecting ANY => %pI4:%d\n",
		&(exp->tuple.dst.u3.ip),
		(int)(ntohs(exp->tuple.dst.u.udp.port))
	); */
	nf_ct_expect_related(exp);
#endif //FILTER_SRC

	nf_ct_expect_put(exp);
	nf_ct_refresh(ct, skb, timeout * HZ);

out:
	return NF_ACCEPT;
}

static struct nf_conntrack_expect_policy exp_policy = {
	.max_expected	= 0,
	.timeout        = 4
};

static struct nf_conntrack_helper helper __read_mostly = {
	.name                 = "upnp",
	.tuple.src.l3num      = AF_INET,
	.tuple.dst.protonum   = IPPROTO_UDP,
	.tuple.src.u.udp.port = cpu_to_be16(UPNP_PORT),
	.me                   = THIS_MODULE,
	.help                 = nf_conntrack_upnp_help,
	.expect_policy        = &exp_policy,
};

static int __init nf_conntrack_upnp_init(void)
{
	printk(KERN_INFO "upnp: %s(%d)\n", __FUNCTION__, __LINE__);
	return nf_conntrack_helper_register(&helper);
}

static void __exit nf_conntrack_upnp_fini(void)
{
	printk(KERN_INFO "upnp: %s(%d)\n", __FUNCTION__, __LINE__);
	nf_conntrack_helper_unregister(&helper);
}

module_init(nf_conntrack_upnp_init);
module_exit(nf_conntrack_upnp_fini);
