/*
 * swconfig_ubnt.c: swconfig UBNT extension
 * Copyright 2019 Ubiquiti Networks, Inc.
 */

#ifndef __SWCONFIG_UBNT_EXT_H
#define __SWCONFIG_UBNT_EXT_H

#ifdef CONFIG_SWCONFIG_UBNT_EXT

#include <linux/types.h>
#include <linux/list.h>
#include <uapi/linux/switch.h>

#define UEXT_ARL_LIST_HASHSIZE 32
#define UEXT_ARL_LIST_HASH(addr) \
	((((const uint8_t *)(addr))[0] ^ ((const uint8_t *)(addr))[5]) % UEXT_ARL_LIST_HASHSIZE)
#define UEXT_ARL_READ_WORK_DELAY_MS 10000
#define UEXT_ARL_DEFAULT_AGE_TIME_S 300

#define MACSTR "%02x:%02x:%02x:%02x:%02x:%02x"
#define MAC2STR(addr) addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]


/**
 * @brief Forward declaration
 */
struct switch_dev;
struct switch_attr;
struct switch_val;

/**
 * @brief Iterator initialization
 */
enum uext_arl_iter_op {
	UEXT_ARL_IT_BEGIN,
	UEXT_ARL_IT_NEXT,
	UEXT_ARL_IT_END
};

/**
 * @brief iterator structure
 */
struct uext_arl_iter {
	void *data;
	uint32_t counter;
	enum uext_arl_iter_op op;
};

/**
 * @brief LUT entry
 */
struct uext_arl_lut_entry {
	/**
	 * @note
	 * uext expects ARL entry's mac address in big endian
	 */
	uint8_t mac[ETH_ALEN];
	uint8_t portmap;
};

/**
 * @brief Cache entry
 */
struct uext_arl_cache_entry {
	struct list_head list;
	unsigned long last_seen;
	unsigned long first_add;
	struct uext_arl_lut_entry lut_e;
};

/**
 * @brief ARL cache
 */
struct uext_arl_cache {
	struct list_head table[UEXT_ARL_LIST_HASHSIZE];
	struct proc_dir_entry *procfs_full;
	struct proc_dir_entry *procfs_fast;
	void *private;
	struct mutex lock;
	struct delayed_work worker;
	uint32_t age_time;
	uint8_t dev_ports;
};

/**
 * @brief UBNT extension structure
 */
struct uext {
	/* procfs entry */
	struct proc_dir_entry *procfs;

	/* ARL cache */
	struct uext_arl_cache *arl_cache;
};

/**
 * @brief UBNT extension structure
 */
struct uext_ops {
	int (*get_arl_entry)(struct switch_dev *dev, struct uext_arl_lut_entry *entry_out,
			     struct uext_arl_iter *it);
};

/**
 * @brief Global attribute
 */
/* ATT ( _name, _desc, _enum, _type, _read_handler, _write_handler )   */

#define UBNT_EXT_GLOBAL_ATT(UEXT_ATT)                                                        \
	UEXT_ATT("uext_arl_cache_age_time", "ARL cache age time (secs)", ARL_CACHE_AGE_TIME, \
		 SWITCH_TYPE_INT, swconfig_uext_arl_cache_age_time_get,                      \
		 swconfig_uext_arl_cache_age_time_set)

/* Prototypes */

/**
 * @brief Handle swconfig reset across all caches of ubnt extension
 *
 * @param dev - switch control structure
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_reset(struct switch_dev *dev);

/**
 * @brief Handle swconfig reset across ARL cache
 *
 * @param dev - switch control structure
 * @param port - port ID or -1 for all ports
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_arl_cache_reset(struct switch_dev *dev, int port);

/**
 * @brief Get ARL table
 *
 * @param dev - switch control structure
 * @param attr - switch attribute structure
 * @param val - switch value structure
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_arl_table_get(struct switch_dev *dev, const struct switch_attr *attr,
				struct switch_val *val);

/**
 * @brief Get ARL cache age time
 *
 * @param dev - switch control structure
 * @param attr - switch attribute structure
 * @param val - switch value structure
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_arl_cache_age_time_get(struct switch_dev *dev, const struct switch_attr *attr,
					 struct switch_val *val);

/**
 * @brief Set ARL cache age time
 *
 * @param dev - switch control structure
 * @param attr - switch attribute structure
 * @param val - switch value structure
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_arl_cache_age_time_set(struct switch_dev *dev, const struct switch_attr *attr,
					 struct switch_val *val);

/**
 * @brief Part of register_switch - initialize ubnt extension
 *
 * @param dev - switch control structure
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_register(struct switch_dev *dev);

/**
 * @brief Part of unregister_switch - destroy ubnt extension
 *
 * @param dev - switch control structure
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_unregister(struct switch_dev *dev);

/**
 * @brief Initialize ubnt swconfig extension (uext)
 *
 * @return int - error from errno.h, 0 on success
 */
int swconfig_uext_init(void);

/**
 * @brief Destroy ubnt swconfig extension (uext)
 *
 * @return int - error from errno.h, 0 on success
 */
void swconfig_uext_exit(void);


#else /* CONFIG_SWCONFIG_UBNT_EXT */
#define UBNT_EXT_GLOBAL_ATT(UEXT_ATT)
#endif /*CONFIG_SWCONFIG_UBNT_EXT*/

#endif /* __SWCONFIG_UBNT_EXT_H */
