/*
 *  Atheros AR71xx built-in ethernet mac driver
 *
 *  Copyright (C) 2008-2010 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  Based on Atheros' AG7100 driver
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/debugfs.h>

#include "ag71xx.h"

static struct dentry *ag71xx_debugfs_root;

static int ag71xx_debugfs_generic_open(struct inode *inode, struct file *file)
{
	file->private_data = inode->i_private;
	return 0;
}

void ag71xx_debugfs_update_int_stats(struct ag71xx *ag, u32 status)
{
	if (status)
		ag->debug.int_stats.total++;
	if (status & AG71XX_INT_TX_PS)
		ag->debug.int_stats.tx_ps++;
	if (status & AG71XX_INT_TX_UR0)
		ag->debug.int_stats.tx_ur0++;
	if (status & AG71XX_INT_TX_UR1)
		ag->debug.int_stats.tx_ur1++;
	if (status & AG71XX_INT_TX_UR2)
		ag->debug.int_stats.tx_ur2++;
	if (status & AG71XX_INT_TX_UR3)
		ag->debug.int_stats.tx_ur3++;
	if (status & AG71XX_INT_TX_BE)
		ag->debug.int_stats.tx_be++;
	if (status & AG71XX_INT_RX_PR)
		ag->debug.int_stats.rx_pr++;
	if (status & AG71XX_INT_RX_OF)
		ag->debug.int_stats.rx_of++;
	if (status & AG71XX_INT_RX_BE)
		ag->debug.int_stats.rx_be++;
	if (status & AG71XX_INT_UNK31)
		ag->debug.int_stats.unk31++;
	if (status & AG71XX_INT_RESERVED)
		ag->debug.int_stats.reserved++;
}

static ssize_t read_file_int_stats(struct file *file, char __user *user_buf,
				   size_t count, loff_t *ppos)
{
#define PR_INT_STAT(_label, _field)					\
	len += snprintf(buf + len, sizeof(buf) - len,			\
		"%20s: %10lu\n", _label, ag->debug.int_stats._field);

	struct ag71xx *ag = file->private_data;
	char buf[512];
	unsigned int len = 0;

	PR_INT_STAT("TX Packet Sent", tx_ps);
	PR_INT_STAT("TX Underrun Q0", tx_ur0);
	PR_INT_STAT("TX Underrun Q1", tx_ur1);
	PR_INT_STAT("TX Underrun Q2", tx_ur2);
	PR_INT_STAT("TX Underrun Q3", tx_ur3);
	PR_INT_STAT("TX Bus Error", tx_be);
	PR_INT_STAT("RX Packet Received", rx_pr);
	PR_INT_STAT("RX Overflow", rx_of);
	PR_INT_STAT("RX Bus Error", rx_be);
	PR_INT_STAT("Bit[31]", unk31);
	PR_INT_STAT("Reserved", reserved);
	len += snprintf(buf + len, sizeof(buf) - len, "\n");
	PR_INT_STAT("Total", total);

	return simple_read_from_buffer(user_buf, count, ppos, buf, len);
#undef PR_INT_STAT
}

static const struct file_operations ag71xx_fops_int_stats = {
	.open	= ag71xx_debugfs_generic_open,
	.read	= read_file_int_stats,
	.owner	= THIS_MODULE
};

void ag71xx_debugfs_update_napi_stats(struct ag71xx *ag, int rx, int tx)
{
	struct ag71xx_napi_stats *stats = &ag->debug.napi_stats;

	if (rx) {
		stats->rx_count++;
		stats->rx_packets += rx;
		if (rx <= AG71XX_NAPI_WEIGHT)
			stats->rx[rx]++;
		if (rx > stats->rx_packets_max)
			stats->rx_packets_max = rx;
	}

	if (tx) {
		stats->tx_count++;
		stats->tx_packets += tx;
		if (tx <= AG71XX_NAPI_WEIGHT)
			stats->tx[tx]++;
		if (tx > stats->tx_packets_max)
			stats->tx_packets_max = tx;
	}
}

static ssize_t read_file_napi_stats(struct file *file, char __user *user_buf,
				    size_t count, loff_t *ppos)
{
	struct ag71xx *ag = file->private_data;
	struct ag71xx_napi_stats *stats = &ag->debug.napi_stats;
	char *buf;
	unsigned int buflen;
	unsigned int len = 0;
	unsigned long rx_avg = 0;
	unsigned long tx_avg = 0;
	int ret;
	int i;

	buflen = 2048;
	buf = kmalloc(buflen, GFP_KERNEL);
	if (!buf)
		return -ENOMEM;

	if (stats->rx_count)
		rx_avg = stats->rx_packets / stats->rx_count;

	if (stats->tx_count)
		tx_avg = stats->tx_packets / stats->tx_count;

	len += snprintf(buf + len, buflen - len, "%3s  %10s %10s\n",
			"len", "rx", "tx");

	for (i = 1; i <= AG71XX_NAPI_WEIGHT; i++)
		len += snprintf(buf + len, buflen - len,
				"%3d: %10lu %10lu\n",
				i, stats->rx[i], stats->tx[i]);

	len += snprintf(buf + len, buflen - len, "\n");

	len += snprintf(buf + len, buflen - len, "%3s: %10lu %10lu\n",
			"sum", stats->rx_count, stats->tx_count);
	len += snprintf(buf + len, buflen - len, "%3s: %10lu %10lu\n",
			"avg", rx_avg, tx_avg);
	len += snprintf(buf + len, buflen - len, "%3s: %10lu %10lu\n",
			"max", stats->rx_packets_max, stats->tx_packets_max);
	len += snprintf(buf + len, buflen - len, "%3s: %10lu %10lu\n",
			"pkt", stats->rx_packets, stats->tx_packets);

	ret = simple_read_from_buffer(user_buf, count, ppos, buf, len);
	kfree(buf);

	return ret;
}

static const struct file_operations ag71xx_fops_napi_stats = {
	.open	= ag71xx_debugfs_generic_open,
	.read	= read_file_napi_stats,
	.owner	= THIS_MODULE
};

#define DESC_PRINT_LEN	64

static ssize_t read_file_ring(struct file *file, char __user *user_buf,
			      size_t count, loff_t *ppos,
			      struct ag71xx *ag,
			      struct ag71xx_ring *ring,
			      unsigned desc_reg)
{
	int ring_size = BIT(ring->order);
	int ring_mask = ring_size - 1;
	char *buf;
	unsigned int buflen;
	unsigned int len = 0;
	unsigned long flags;
	ssize_t ret;
	int curr;
	int dirty;
	u32 desc_hw;
	int i;

	buflen = (ring_size * DESC_PRINT_LEN);
	buf = kmalloc(buflen, GFP_KERNEL);
	if (!buf)
		return -ENOMEM;

	len += snprintf(buf + len, buflen - len,
			"Idx ... %-8s %-8s %-8s %-8s .\n",
			"desc", "next", "data", "ctrl");

	spin_lock_irqsave(&ag->lock, flags);

	curr = (ring->curr & ring_mask);
	dirty = (ring->dirty & ring_mask);
	desc_hw = ag71xx_rr(ag, desc_reg);
	for (i = 0; i < ring_size; i++) {
		struct ag71xx_desc *desc = ag71xx_ring_desc(ring, i);
		u32 desc_dma = ((u32) ring->descs_dma) + i * AG71XX_DESC_SIZE;

		len += snprintf(buf + len, buflen - len,
			"%3d %c%c%c %08x %08x %08x %08x %c\n",
			i,
			(i == curr) ? 'C' : ' ',
			(i == dirty) ? 'D' : ' ',
			(desc_hw == desc_dma) ? 'H' : ' ',
			desc_dma,
			desc->next,
			desc->data,
			desc->ctrl,
			(desc->ctrl & DESC_EMPTY) ? 'E' : '*');
	}

	spin_unlock_irqrestore(&ag->lock, flags);

	ret = simple_read_from_buffer(user_buf, count, ppos, buf, len);
	kfree(buf);

	return ret;
}

static ssize_t read_file_tx_ring(struct file *file, char __user *user_buf,
				 size_t count, loff_t *ppos)
{
	struct ag71xx *ag = file->private_data;

	return read_file_ring(file, user_buf, count, ppos, ag, &ag->tx_ring,
			      AG71XX_REG_TX_DESC);
}

static const struct file_operations ag71xx_fops_tx_ring = {
	.open	= ag71xx_debugfs_generic_open,
	.read	= read_file_tx_ring,
	.owner	= THIS_MODULE
};

static ssize_t read_file_rx_ring(struct file *file, char __user *user_buf,
				 size_t count, loff_t *ppos)
{
	struct ag71xx *ag = file->private_data;

	return read_file_ring(file, user_buf, count, ppos, ag, &ag->rx_ring,
			      AG71XX_REG_RX_DESC);
}

static const struct file_operations ag71xx_fops_rx_ring = {
	.open	= ag71xx_debugfs_generic_open,
	.read	= read_file_rx_ring,
	.owner	= THIS_MODULE
};

static ssize_t read_file_tx_delay_enable(struct file *file, char __user *user_buf,
				 size_t count, loff_t *ppos)
{
	struct ag71xx *ag = file->private_data;
	char buf[32];
	unsigned int len;

	len = sprintf(buf, "%u\n", ag->tx_delay_enable);

	return simple_read_from_buffer(user_buf, count, ppos, buf, len);
}

static ssize_t write_file_tx_delay_enable(struct file *file, const char __user *user_buf,
				 size_t count, loff_t *ppos)
{
	struct ag71xx *ag = file->private_data;
	unsigned int val;
	char buf[32];
	ssize_t len;

	len = min(count, sizeof(buf) - 1);
	if (copy_from_user(buf, user_buf, len))
		return -EFAULT;

	buf[len] = '\0';
	if (kstrtouint(buf, 0, &val))
		return -EINVAL;

	ag->tx_delay_enable = val;
	return count;
}

static const struct file_operations ag71xx_fops_tx_delay_enable = {
	.open	= ag71xx_debugfs_generic_open,
	.read	= read_file_tx_delay_enable,
	.write	= write_file_tx_delay_enable,
	.owner	= THIS_MODULE
};

static ssize_t read_file_regs(struct file *file, char __user *user_buf,
				   size_t count, loff_t *ppos)
{
#define PR_REG_H(_label, _reg)					\
	len += snprintf(buf + len, buflen - len,		\
		"%-20s 0x%08x\n", _label, __raw_readl(ag->mac_base + _reg));
#define PR_REG_D(_label, _reg)					\
	len += snprintf(buf + len, buflen - len,		\
		"%-20s %10u\n", _label, __raw_readl(ag->mac_base + _reg));

	struct ag71xx *ag = file->private_data;
	char *buf;
	unsigned int buflen;
	unsigned int len = 0;
	unsigned long flags;
	ssize_t ret;

	buflen = 4096;
	buf = kmalloc(buflen, GFP_KERNEL);
	if (!buf)
		return -ENOMEM;

	spin_lock_irqsave(&ag->lock, flags);

	len += snprintf(buf + len, buflen - len, "Config:\n");
	PR_REG_H("MAC_CFG1", AG71XX_REG_MAC_CFG1);	/* 0x000 - MAC Configuration 1 */
	PR_REG_H("MAC_CFG2", AG71XX_REG_MAC_CFG2);	/* 0x004 - MAC Configuration 2 */
	PR_REG_H("MAC_IPG", AG71XX_REG_MAC_IPG);	/* 0x008 - IPG/IFG */
	PR_REG_H("MAC_HDX", AG71XX_REG_MAC_HDX);	/* 0x00C - Half-Duplex */
	PR_REG_H("MAC_MFL", AG71XX_REG_MAC_MFL);	/* 0x010 - Maximum Frame Length */

	PR_REG_H("MII_CFG", AG71XX_REG_MII_CFG);	/* 0x020 - MII Configuration */
	PR_REG_H("MII_CMD", AG71XX_REG_MII_CMD);	/* 0x024 - MII Command */
	PR_REG_H("MII_ADDR", AG71XX_REG_MII_ADDR);	/* 0x028 - MII Address */
	PR_REG_H("MII_CTRL", AG71XX_REG_MII_CTRL);	/* 0x02C - MII Control */
	PR_REG_H("MII_STATUS", AG71XX_REG_MII_STATUS);	/* 0x030 - MII Status */
	PR_REG_H("MII_IND", AG71XX_REG_MII_IND);	/* 0x034 - MII Indicators */

	PR_REG_H("MAC_IFCTL", AG71XX_REG_MAC_IFCTL);	/* 0x038 - Interface Control */
	PR_REG_H("MAC_IFSTATUS", 0x03C);		/* 0x03C - Interface Status */
	PR_REG_H("MAC_ADDR1", AG71XX_REG_MAC_ADDR1);	/* 0x040 - STA Address 1 */
	PR_REG_H("MAC_ADDR2", AG71XX_REG_MAC_ADDR2);	/* 0x044 - STA Address 2 */

	PR_REG_H("FIFO_CFG0", AG71XX_REG_FIFO_CFG0);	/* 0x048 - ETH Configuration 0 */
	PR_REG_H("FIFO_CFG1", AG71XX_REG_FIFO_CFG1);	/* 0x04C - ETH Configuration 1 */
	PR_REG_H("FIFO_CFG2", AG71XX_REG_FIFO_CFG2);	/* 0x050 - ETH Configuration 2 */
	PR_REG_H("FIFO_CFG3", AG71XX_REG_FIFO_CFG3);	/* 0x054 - ETH Configuration 3 */
	PR_REG_H("FIFO_CFG4", AG71XX_REG_FIFO_CFG4);	/* 0x058 - ETH Configuration 4 */
	PR_REG_H("FIFO_CFG5", AG71XX_REG_FIFO_CFG5);	/* 0x05C - ETH Configuration 5 */

	len += snprintf(buf + len, buflen - len, "\nStats:\n");
	PR_REG_D("TR64",  0x080);			/* 0x080 - Tx/Rx 64 Byte Frame Counter */
	PR_REG_D("TR127", 0x084);			/* 0x084 - Tx/Rx 65-127 Byte Frame Counter */
	PR_REG_D("TR255", 0x088);			/* 0x088 - Tx/Rx 128-255 Byte Frame Counter */
	PR_REG_D("TR511", 0x08C);			/* 0x08C - Tx/Rx 256-511 Byte Frame Counter */
	PR_REG_D("TR1K",  0x090);			/* 0x090 - Tx/Rx 512-1023 Byte Frame Counter */
	PR_REG_D("TRMAX", 0x094);			/* 0x094 - Tx/Rx 1024-1518 Byte Frame Counter */
	PR_REG_D("TRMGV", 0x098);			/* 0x098 - Tx/Rx 1519-1522 Byte VLAN Frame Counter */

	PR_REG_D("RBYT", 0x09C);			/* 0x09C - Receive Byte Counter */
	PR_REG_D("RPKT", 0x0A0);			/* 0x0A0 - Receive Packet Counter */
	PR_REG_D("RFCS", 0x0A4);			/* 0x0A4 - Receive FCS Error Counter */
	PR_REG_D("RMCA", 0x0A8);			/* 0x0A8 - Receive Multicast Packet Counter */
	PR_REG_D("RBCA", 0x0AC);			/* 0x0AC - Receive Broadcast Packet Counter */
	PR_REG_D("RXCF", 0x0B0);			/* 0x0B0 - Receive Control Frame Packet Counter */
	PR_REG_D("RXPF", 0x0B4);			/* 0x0B4 - Receive Pause Frame Packet Counter */
	PR_REG_D("RXUO", 0x0B8);			/* 0x0B8 - Receive Unknown OPCode Packet Counter */
	PR_REG_D("RALN", 0x0BC);			/* 0x0BC - Receive Alignment Error Counter */
	PR_REG_D("RFLR", 0x0C0);			/* 0x0C0 - Receive Frame Length Error Counter */
	PR_REG_D("RCDE", 0x0C4);			/* 0x0C4 - Receive Code Error Counter */
	PR_REG_D("RCSE", 0x0C8);			/* 0x0C8 - Receive Carrier Sense Error Counter */
	PR_REG_D("RUND", 0x0CC);			/* 0x0CC - Receive Undersize Packet Counter */
	PR_REG_D("ROVR", 0x0D0);			/* 0x0D0 - Receive Oversize Packet Counter */
	PR_REG_D("RFRG", 0x0D4);			/* 0x0D4 - Receive Fragments Counter */
	PR_REG_D("RJBR", 0x0D8);			/* 0x0D8 - Receive Jabber Counter */
	PR_REG_D("RDRP", 0x0DC);			/* 0x0DC - Receive Dropped Packet Counter */

	PR_REG_D("TBYT", 0x0E0);			/* 0x0E0 - Transmit Byte Counter */
	PR_REG_D("TPKT", 0x0E4);			/* 0x0E4 - Transmit Packet Counter */
	PR_REG_D("TMCA", 0x0E8);			/* 0x0E8 - Transmit Multicast Packet Counter */
	PR_REG_D("TBCA", 0x0EC);			/* 0x0EC - Transmit Broadcast Packet Counter */
	PR_REG_D("TXPF", 0x0F0);			/* 0x0F0 - Transmit Pause Control Frame Counter */
	PR_REG_D("TDFR", 0x0F4);			/* 0x0F4 - Transmit Deferral Packet Counter */
	PR_REG_D("TEDF", 0x0F8);			/* 0x0F8 - Transmit Excessive Deferral Packet Counter */
	PR_REG_D("TSCL", 0x0FC);			/* 0x0FC - Transmit Single Collision Packet Counter */
	PR_REG_D("TMCL", 0x100);			/* 0x100 - Transmit Multiple Collision Packet */
	PR_REG_D("TLCL", 0x104);			/* 0x104 - Transmit Late Collision Packet Counter */
	PR_REG_D("TXCL", 0x108);			/* 0x108 - Transmit Excessive Collision Packet Counter */
	PR_REG_D("TNCL", 0x10C);			/* 0x10C - Transmit Total Collision Counter */
	PR_REG_D("TPFH", 0x110);			/* 0x110 - Transmit Pause Frames Honored Counter */
	PR_REG_D("TDRP", 0x114);			/* 0x114 - Transmit Drop Frame Counter */
	PR_REG_D("TJBR", 0x118);			/* 0x118 - Transmit Jabber Frame Counter */
	PR_REG_D("TFCS", 0x11C);			/* 0x11C - Transmit FCS Error Counter */
	PR_REG_D("TXCF", 0x120);			/* 0x120 - Transmit Control Frame Counter */
	PR_REG_D("TOVR", 0x124);			/* 0x124 - Transmit Oversize Frame Counter */
	PR_REG_D("TUND", 0x128);			/* 0x128 - Transmit Undersize Frame Counter */
	PR_REG_D("TFRG", 0x12C);			/* 0x12C - Transmit Fragment Counter */

	len += snprintf(buf + len, buflen - len, "\nMisc:\n");
	PR_REG_H("CAR1", 0x130);			/* 0x130 - Carry Register 1 */
	PR_REG_H("CAR2", 0x134);			/* 0x134 - Carry Register 2 */
	PR_REG_H("CAM1", 0x138);			/* 0x138 - Carry Mask Register 1 */
	PR_REG_H("CAM2", 0x13C);			/* 0x13C - Carry Mask Register 2 */

	PR_REG_H("TX_CTRL", AG71XX_REG_TX_CTRL);	/* 0x180 - DMA Transfer Control for Queue 0 */
	PR_REG_H("TX_DESC", AG71XX_REG_TX_DESC);	/* 0x184 - Descriptor Address for Queue 0 Tx */
	PR_REG_H("TX_STATUS", AG71XX_REG_TX_STATUS);	/* 0x188 - DMA Tx Status */
	PR_REG_H("RX_CTRL", AG71XX_REG_RX_CTRL);	/* 0x18C - Rx Control */
	PR_REG_H("RX_DESC", AG71XX_REG_RX_DESC);	/* 0x190 - Pointer to Rx Descriptor */
	PR_REG_H("RX_STATUS", AG71XX_REG_RX_STATUS);	/* 0x194 - Rx Status */
	PR_REG_H("INT_ENABLE", AG71XX_REG_INT_ENABLE);	/* 0x198 - Interrupt Mask */
	PR_REG_H("INT_STATUS", AG71XX_REG_INT_STATUS);	/* 0x19C - Interrupts */
	PR_REG_H("ETH_TX_BURST", 0x1A0);		/* 0x1A0 - Ethernet Tx burst */
	PR_REG_H("ETH_TXFIFO_TH", 0x1A4);		/* 0x1A4 - Ethernet Tx FIFO Max and Min Threshold */
	PR_REG_H("FIFO_DEPTH", AG71XX_REG_FIFO_DEPTH);	/* 0x1A8 - Current Tx and Rx FIFO Depth */
	PR_REG_H("ETH_RXFIFO_TH", 0x1AC);		/* 0x1AC - Ethernet Rx FIFO */
	PR_REG_H("RX_SM", AG71XX_REG_RX_SM);		/* 0x1B0 - Undocumented - used for DMA stuck detection */
	PR_REG_H("TX_SM", AG71XX_REG_TX_SM);		/* 0x1B4 - Undocumented - used for DMA stuck detection */
	PR_REG_H("ETH_FREE_TIMER", 0x1B8);		/* 0x1B8 - Ethernet Free Timer */
	PR_REG_H("[0x1BC]", 0x1BC);			/* 0x1BC - ??? */
	PR_REG_H("DMATXCNTRL_Q1", 0x1C0);		/* 0x1C0 - DMA Transfer Control for Queue 1 */
	PR_REG_H("DMATXDESCR_Q1", 0x1C4);		/* 0x1C4 - Descriptor Address for Queue 1 Tx */
	PR_REG_H("DMATXCNTRL_Q2", 0x1C8);		/* 0x1C8 - DMA Transfer Control for Queue 2 */
	PR_REG_H("DMATXDESCR_Q2", 0x1CC);		/* 0x1CC - Descriptor Address for Queue 2 Tx */
	PR_REG_H("DMATXCNTRL_Q3", 0x1D0);		/* 0x1D0 - DMA Transfer Control for Queue 3 */
	PR_REG_H("DMATXDESCR_Q3", 0x1D4);		/* 0x1D4 - Descriptor Address for Queue 3 Tx */
	PR_REG_H("DMATXARBCFG", 0x1D8);			/* 0x1D8 - DMA Tx Arbitration Configuration */
	PR_REG_H("[0x1DC]", 0x1DC);			/* 0x1DC - ??? */
	PR_REG_H("[0x1E0]", 0x1E0);			/* 0x1E0 - ??? */
	PR_REG_H("DMATXSTATUS_123", 0x1E4);		/* 0x1E4 - Tx Status and Packet Count for Queues 1 to 3 */

	PR_REG_H("LCL_MAC_ADDR_DW0", 0x200);		/* 0x200 - Local MAC Address Dword0 */
	PR_REG_H("LCL_MAC_ADDR_DW1", 0x204);		/* 0x204 - Local MAC Address Dword1 */
	PR_REG_H("NXT_HOP_DST_ADDR_DW0", 0x208);	/* 0x208 - Next Hop Router MAC Address Dword0 */
	PR_REG_H("NXT_HOP_DST_ADDR_DW1", 0x20C);	/* 0x20C - Next Hop Router MAC Destination Address Dword1 */
	PR_REG_H("GLOBAL_IP_ADDR0", 0x210);		/* 0x210 - Local Global IP Address 0 */
	PR_REG_H("GLOBAL_IP_ADDR1", 0x214);		/* 0x214 - Local Global IP Address 1 */
	PR_REG_H("GLOBAL_IP_ADDR2", 0x218);		/* 0x218 - Local Global IP Address 2 */
	PR_REG_H("GLOBAL_IP_ADDR3", 0x21C);		/* 0x21C - Local Global IP Address 3 */

	spin_unlock_irqrestore(&ag->lock, flags);

	ret = simple_read_from_buffer(user_buf, count, ppos, buf, len);
	kfree(buf);

	return ret;
#undef PR_REG_H
#undef PR_REG_D
}

static const struct file_operations ag71xx_fops_regs = {
	.open	= ag71xx_debugfs_generic_open,
	.read	= read_file_regs,
	.owner	= THIS_MODULE
};

void ag71xx_debugfs_exit(struct ag71xx *ag)
{
	debugfs_remove_recursive(ag->debug.debugfs_dir);
}

int ag71xx_debugfs_init(struct ag71xx *ag)
{
	struct device *dev = &ag->pdev->dev;

	ag->debug.debugfs_dir = debugfs_create_dir(dev_name(dev),
						   ag71xx_debugfs_root);
	if (!ag->debug.debugfs_dir) {
		dev_err(dev, "unable to create debugfs directory\n");
		return -ENOENT;
	}

	debugfs_create_file("int_stats", S_IRUGO, ag->debug.debugfs_dir,
			    ag, &ag71xx_fops_int_stats);
	debugfs_create_file("napi_stats", S_IRUGO, ag->debug.debugfs_dir,
			    ag, &ag71xx_fops_napi_stats);
	debugfs_create_file("tx_ring", S_IRUGO, ag->debug.debugfs_dir,
			    ag, &ag71xx_fops_tx_ring);
	debugfs_create_file("rx_ring", S_IRUGO, ag->debug.debugfs_dir,
			    ag, &ag71xx_fops_rx_ring);
	debugfs_create_file("regs", S_IRUGO, ag->debug.debugfs_dir,
			    ag, &ag71xx_fops_regs);
	debugfs_create_file("tx_delay_enable", S_IRUGO, ag->debug.debugfs_dir,
			    ag, &ag71xx_fops_tx_delay_enable);

	return 0;
}

int ag71xx_debugfs_root_init(void)
{
	if (ag71xx_debugfs_root)
		return -EBUSY;

	ag71xx_debugfs_root = debugfs_create_dir(KBUILD_MODNAME, NULL);
	if (!ag71xx_debugfs_root)
		return -ENOENT;

	return 0;
}

void ag71xx_debugfs_root_exit(void)
{
	debugfs_remove(ag71xx_debugfs_root);
	ag71xx_debugfs_root = NULL;
}
