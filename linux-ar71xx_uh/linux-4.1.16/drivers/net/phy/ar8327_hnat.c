#define AR8327_REG_ACL_FUNC0			0x0400
#define   AR8327_ACL_FUNC_INDEX			BITS(0,7)
#define   AR8327_ACL_RULE_SEL_S			8
#define   AR8327_ACL_RULE_SEL			BITS(AR8327_ACL_RULE_SEL_S,2)
#define   AR8327_ACL_RULE_SEL_RULE		0
#define   AR8327_ACL_RULE_SEL_MASK		1
#define   AR8327_ACL_RULE_SEL_RESULT		2
#define   AR8327_ACL_FUNC			BIT(10)
#define   AR8327_ACL_FUNC_WRITE			0
#define   AR8327_ACL_FUNC_READ			AR8327_ACL_FUNC
#define   AR8327_ACL_BUSY			BIT(31)

#define AR8327_REG_ACL_FUNC1			0x0404
#define AR8327_REG_ACL_FUNC2			0x0408
#define AR8327_REG_ACL_FUNC3			0x040c
#define AR8327_REG_ACL_FUNC4			0x0410

#define AR8327_REG_ACL_FUNC5			0x0414
#define   AR8327_ACL_RULE_DATA4			BITS(0,8)

#define AR8327_REG_PRIVATE_IP_CTRL		0x0418

#define AR8327_REG_ROUTER_EG_VLAN_MODE		0x0c80
#define   AR8327_ROUTER_EG_VLAN_MODE_PORT(_i)	((_i) * 4)

#define AR8327_L3_HROUTER_CTRL			0x0e00
#define   AR8327_HROUTER_CTRL_ARP_OWR_EN	BIT(18)
#define   AR8327_HROUTER_CTRL_ARP_LEARN_MODE	BIT(19)

#define AR8327_L3_HROUTER_PCTRL0		0x0e04
#define AR8327_L3_HROUTER_PCTRL1		0x0e08

#define AR8327_L3_HROUTER_PCTRL2		0x0e0c
#define   AR8327_HROUTER_PCTRL2_ARP_REQ_LEARN	BITS(0,7)
#define   AR8327_HROUTER_PCTRL2_ARP_REQ_LEARN_S	0
#define   AR8327_HROUTER_PCTRL2_ARP_REP_LEARN	BITS(8,7)
#define   AR8327_HROUTER_PCTRL2_ARP_REP_LEARN_S	8
#define   AR8327_HROUTER_PORT(_i)		BIT(_i)

#define AR8327_L3_ARP_USED_COUNT		0x0e34

#define AR8327_L3_HNAT_CTRL			0x0e38
#define   AR8327_HNAT_CTRL_HNAPT_EN		BIT(0)
#define   AR8327_HNAT_CTRL_HNAT_EN		BIT(1)
#define   AR8327_HNAT_CTRL_NAPT_MODE		BITS(2,2)
#define   AR8327_HNAT_CTRL_NAPT_MODE_S		2
#define   AR8327_HNAT_CTRL_NAPT_MODE_FULL_CONE		0
#define   AR8327_HNAT_CTRL_NAPT_MODE_STRICT_CONE	1
#define   AR8327_HNAT_CTRL_NAPT_MODE_PORT_STRICT	2
#define   AR8327_HNAT_CTRL_NAPT_OVERWRITE_EN	BIT(4)
#define   AR8327_HNAT_CTRL_NAT_HASH_MODE	BITS(5,2)
#define   AR8327_HNAT_CTRL_NAPT_AGE_MODE	BIT(7)
#define   AR8327_HNAT_CTRL_NAPT_AGE_TIMER	BITS(8,8)
#define   AR8327_HNAT_CTRL_NAPT_TCP_AGE_STEP	BITS(16,2)
#define   AR8327_HNAT_CTRL_NAPT_UDP_AGE_STEP	BITS(18,2)
#define   AR8327_HNAT_CTRL_NAPT_AGE_SUP_THR	BITS(20,2)
#define   AR8327_HNAT_CTRL_NAPT_AGE_SUP_STEP	BIT(22)

#define AR8327_L3_NAPT_USED_COUNT		0x0e44
#define AR8327_L3_ENTRY_EDIT_DATA0		0x0e48
#define AR8327_L3_ENTRY_EDIT_DATA1		0x0e4c
#define AR8327_L3_ENTRY_EDIT_DATA2		0x0e50
#define AR8327_L3_ENTRY_EDIT_DATA3		0x0e54

#define AR8327_L3_ENTRY_EDIT_CONTROL_REG	0x0e58
#define AR8327_L3_EECR_ENTRY_FUNC		BITS(0,3)
#define   AR8327_L3_EECR_ENTRY_FUNC_NOP		0
#define   AR8327_L3_EECR_ENTRY_FUNC_FLUSH	1
#define   AR8327_L3_EECR_ENTRY_FUNC_ADD		2
#define   AR8327_L3_EECR_ENTRY_FUNC_DEL		3
#define   AR8327_L3_EECR_ENTRY_FUNC_NEXT	4
#define   AR8327_L3_EECR_ENTRY_FUNC_SEARCH	5
#define   AR8327_L3_EECR_TABLE_SEL_S		4
#define   AR8327_L3_EECR_TABLE_SEL		BITS(AR8327_L3_EECR_TABLE_SEL_S,2)
#define   AR8327_L3_EECR_TABLE_NAPT		0
#define   AR8327_L3_EECR_TABLE_RESERVED		1
#define   AR8327_L3_EECR_TABLE_NAT		2
#define   AR8327_L3_EECR_TABLE_ARP		3
#define   AR8327_L3_EECR_STATUS			BIT(7)
#define   AR8327_L3_EECR_CMD_INDEX_S		8
#define   AR8327_L3_EECR_CMD_INDEX		BITS(AR8327_L3_EECR_CMD_INDEX_S,10)
#define   AR8327_L3_EECR_SPECIFIC_AGE_EN	BIT(18)
#define   AR8327_L3_EECR_SPECIFIC_SIP_EN	BIT(19)
#define   AR8327_L3_EECR_SPECIFIC_PIP_EN	BIT(20)
#define   AR8327_L3_EECR_SPECIFIC_VID_EN	BIT(21)
#define   AR8327_L3_EECR_SPECIFIC_SP_EN		BIT(22)
#define   AR8327_L3_EECR_BUSY			BIT(31)

#define AR8327_L3_PRIV_BASE_IP_ADDR		0x0e5c

/* ACL action word 0 (10 in overall) */
#define AR8327_ACL_ACT_STAG			BITS(0,16)
#define AR8327_ACL_ACT_CTAG			BITS(16,16)
/* ACL action word 1 (11 in overall) */
#define AR8327_ACL_ACT_DSCP			BITS(0,6)
#define AR8327_ACL_ACT_DSCP_REMAP_EN		BIT(6)
#define AR8327_ACL_ACT_STAG_PRI_REMAP_EN	BIT(7)
#define AR8327_ACL_ACT_STAG_DEI_CHANGE_EN	BIT(8)
#define AR8327_ACL_ACT_CTAG_PRI_REMAP_EN	BIT(9)
#define AR8327_ACL_ACT_CTAG_DEI_CHANGE_EN	BIT(10)
#define AR8327_ACL_ACT_TRANS_STAG_CHANGE_EN	BIT(11)
#define AR8327_ACL_ACT_TRANS_CTAG_CHANGE_EN	BIT(12)
#define AR8327_ACL_ACT_LOOKUP_VID_CHANGE_EN	BIT(13)
#define AR8327_ACL_ACT_FORCE_L3_MODE		BITS(14,2)
#define AR8327_ACL_ACT_FORCE_L3_MODE_S		14
#define AR8327_ACL_ACT_FORCE_L3_MODE_NOFORCE	0
#define AR8327_ACL_ACT_FORCE_L3_MODE_SNAT	1
#define AR8327_ACL_ACT_FORCE_L3_MODE_DNAT	2
#define AR8327_ACL_ACT_ARP_INDEX_OVER_EN	BIT(16)
#define AR8327_ACL_ACT_ARP_INDEX		BITS(17,7)
#define AR8327_ACL_ACT_ARP_INDEX_S		17
#define AR8327_ACL_ACT_ARP_WCMP			BIT(24)
#define AR8327_ACL_ACT_ENQUEUE_PRI		BITS(25,3)
#define AR8327_ACL_ACT_ENQUEUE_PRI_OVER_EN	BIT(28)

#define AR8327_HNAT_NAPT_MAX_ENT	1024
#define AR8327_HNAT_ARP_MAX_ENT		128
#define AR8327_ACL_MAX_ENT		96
#define AR8327_MAC_MAX_ENT		8

#define AR8327_ATHEROS_HEADER		0xaaaa
#define AR8327_ATHEROS_HEADER_LOW	(AR8327_ATHEROS_HEADER & 0xff)
#define AR8327_ATHEROS_HEADER_HIGH	(AR8327_ATHEROS_HEADER >> 8)

#define ar8327_hnat_napt_is_port(_p)	(sp == _p || dp == _p || \
					 xp == _p || dxp == _p)

extern struct list_head *switch_devs;
extern spinlock_t *switch_devs_lock;

u32 ar8327_hnat_wan_ip = 0;
u32 ar8327_hnat_war_init_done = 0;
u32 ar8327_hnat_arp_lan_ip = 0;
u8 ar8327_hnat_arp_lan_mac[6] = {0};
u32 ar8327_hnat_arp_wan_ip = 0;
u8 ar8327_hnat_arp_wan_mac[6] = {0};
u32 ar8327_hnat_arp_gw_ip = 0;
u8 ar8327_hnat_arp_gw_mac[6] = {0};

u8 ar8327_hnat_mac_idx_lan = 0;
u16 ar8327_hnat_mac_vid_lan = 1;
u8 ar8327_hnat_mac_idx_wan = 1;
u16 ar8327_hnat_mac_vid_wan = 2;


static int
ar8327_hnat_is_en(struct ar8xxx_priv *priv)
{
	u32 v;
	int ret = 0;

	if (!priv)
		return ret;

	v = ar8xxx_read(priv, AR8327_L3_HNAT_CTRL);
	if (!(v & AR8327_HNAT_CTRL_HNAT_EN) || !(v & AR8327_HNAT_CTRL_HNAPT_EN))
		goto exit;

	v = ar8xxx_read(priv, AR8327_REG_MODULE_EN);
	if (!(v & AR8327_MODULE_EN_L3))
		goto exit;

	if (priv->phy->attached_dev->eth_mangle_rx)
		ret = 1;

exit:
	return ret;
}

static int
ar8327_l3_table_write(struct ar8xxx_priv *priv, u32 *data,
				int table, int func, u32 opt)
{
	u32 v;

	/* FIXME: should we reset if hw is busy already? */
	/* return early if hw is busy before trying to write l3 table */
	v = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_CONTROL_REG);
	if (v & AR8327_L3_EECR_BUSY)
		return -1;

	if (data) {
		ar8xxx_write(priv, AR8327_L3_ENTRY_EDIT_DATA0, data[0]);
		ar8xxx_write(priv, AR8327_L3_ENTRY_EDIT_DATA1, data[1]);
		ar8xxx_write(priv, AR8327_L3_ENTRY_EDIT_DATA2, data[2]);
		ar8xxx_write(priv, AR8327_L3_ENTRY_EDIT_DATA3, data[3]);
	}

	ar8xxx_write(priv, AR8327_L3_ENTRY_EDIT_CONTROL_REG,
				AR8327_L3_EECR_BUSY | opt | func |
				(table << AR8327_L3_EECR_TABLE_SEL_S));

	/* check for busy flag */
	if (ar8216_wait_bit_times(priv, AR8327_L3_ENTRY_EDIT_CONTROL_REG,
					AR8327_L3_EECR_BUSY, 0, 25, 1000))
		return -1;

	/* from ssdk: hw requirement, should read busy again (wtf?) */
	v = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_CONTROL_REG);
	if (v & AR8327_L3_EECR_BUSY)
		return -1;

	return 0;
}

static int
ar8327_hnat_napt(u32 sip, u32 dip, u32 xip, u32 dxip,
			u32 sp, u32 dp, u32 xp, u32 dxp,
			u32 proto, u32 func)
{
	struct switch_dev *dev = NULL;
	struct switch_dev *p;
	struct ar8xxx_priv *priv = NULL;
	int dnat = 0;
	u32 ar8327_hnat_proto = -1;
	u32 v;
	u32 data[4] = {0};

	/* only TCP (0) and UDP (1) are supported */
	switch (proto) {
	case IPPROTO_TCP:
		ar8327_hnat_proto = 0;
		break;
	case IPPROTO_UDP:
		ar8327_hnat_proto = 1;
		/* ipsec is not supported */
		if (ar8327_hnat_napt_is_port(500) ||
		    ar8327_hnat_napt_is_port(4500) ||
		    ar8327_hnat_napt_is_port(1701))
			return -1;
		break;
	default:
		return -1;
	}

	spin_lock_bh(switch_devs_lock);
	list_for_each_entry(p, switch_devs, dev_list) {
		if (p->id != 1)
			continue;

		dev = p;
		break;
	}
	if (dev)
		priv = swdev_to_ar8xxx(dev);
	spin_unlock_bh(switch_devs_lock);

	if (!priv)
		return -1;

	v = ar8xxx_read(priv, AR8327_L3_NAPT_USED_COUNT);

	switch (func) {
	case AR8327_L3_EECR_ENTRY_FUNC_ADD:
		if (!(ar8327_hnat_is_en(priv)) || v >= AR8327_HNAT_NAPT_MAX_ENT)
			return -1;
		break;
	case AR8327_L3_EECR_ENTRY_FUNC_DEL:
		if (!v)
			return -1;
		break;
	default:
		return -1;
	}

	if (ar8327_hnat_wan_ip == dip)
		dnat = 1;

	/* SNAT: WAN host destination IP (DIP)
	 * DNAT: WAN host source IP (SIP) */
	data[0] = dnat? sip : dip;
	/* source and destination ports */
	data[1] = (dnat? sp : dp) |
		((dnat? dxp : sp) << 16);
	/* router WAN IP index (always 0)
	 * translated port, LAN host IP 12 last bits */
	data[2] = (dnat? dp : xp) |
		(((dnat? dxip : sip) & 0xfff) << 20);
	data[3] = 0x0000f000 | /* static entry */
		((ar8327_hnat_proto & 0x3) << 2) |
		0x3; /* action=forward */
	/* add one NAPT entry */
	if (ar8327_l3_table_write(priv, data,
					AR8327_L3_EECR_TABLE_NAPT,
					func, 0))
		return -1;

	return 0;
}

int
ar8327_hnat_napt_add(u32 sip, u32 dip, u32 xip, u32 dxip,
			u32 sp, u32 dp, u32 xp, u32 dxp,
			u32 proto)
{
	return ar8327_hnat_napt(sip, dip, xip, dxip,
				sp, dp, xp, dxp,
				proto, AR8327_L3_EECR_ENTRY_FUNC_ADD);
}
EXPORT_SYMBOL(ar8327_hnat_napt_add);

int
ar8327_hnat_napt_del(u32 sip, u32 dip, u32 xip, u32 dxip,
			u32 sp, u32 dp, u32 xp, u32 dxp,
			u32 proto)
{
	return ar8327_hnat_napt(sip, dip, xip, dxip,
				sp, dp, xp, dxp,
				proto, AR8327_L3_EECR_ENTRY_FUNC_DEL);
}
EXPORT_SYMBOL(ar8327_hnat_napt_del);

static void
ar8327_mangle_rx(struct net_device *dev, struct sk_buff *skb)
{
	struct ar8xxx_priv *priv;
	unsigned char *buf;
	int port, vlan;

	priv = dev->phy_ptr;
	if (!priv)
		return;

	/* don't strip the header if vlan mode is disabled */
	if (!priv->vlan)
		return;

	buf = skb->data;

	/* check for custom ethertype presence */
	if ((buf[12] != AR8327_ATHEROS_HEADER_LOW) ||
			(buf[13] != AR8327_ATHEROS_HEADER_HIGH))
		return;

	/* check for VLAN header presence */
	if ((buf[16] == 0x81) && (buf[17] == 0x00)) {
		memmove(buf + 4, buf, 12);
		skb_pull(skb, 4);
		return;
	}

	/* Atheros header indicates original ingress port */
	port = buf[15] & 0x7;

	/* lookup port vid from local table */
	vlan = priv->vlan_id[priv->pvid[port]];

	/* replace custom ethertype with VLAN */
	buf[12] = 0x81;
	buf[13] = 0x00;

	/* replace Atheros header with ingress port VLAN ID */
	buf[14] = vlan >> 8;
	buf[15] = vlan & 0xff;
}

static void
ar8327_arp_learn_en(struct ar8xxx_priv *priv, bool enable)
{
	u32 frame_ack_arp = 0x60606060;
	u32 frame_ack_ctrl0, frame_ack_ctrl1;
	u32 fwd_ctrl0, hrouter_ctrl, hrouter_pctrl2;

	frame_ack_ctrl0 = ar8xxx_read(priv, AR8327_REG_FRAME_ACK_CTRL0);
	frame_ack_ctrl0 &= ~frame_ack_arp;
	frame_ack_ctrl1 = ar8xxx_read(priv, AR8327_REG_FRAME_ACK_CTRL1);
	frame_ack_ctrl1 &= ~(frame_ack_arp >> 8);

	fwd_ctrl0 = ar8xxx_read(priv, AR8327_REG_FWD_CTRL0);
	fwd_ctrl0 &= ~(AR8327_FWD_CTRL0_ARP_FWD_ACT_FORWARD <<
			AR8327_FWD_CTRL0_ARP_FWD_ACT_S);

	hrouter_ctrl = ar8xxx_read(priv, AR8327_L3_HROUTER_CTRL);
	hrouter_ctrl &= ~(AR8327_HROUTER_CTRL_ARP_OWR_EN |
				AR8327_HROUTER_CTRL_ARP_LEARN_MODE);

	hrouter_pctrl2 = ar8xxx_read(priv, AR8327_L3_HROUTER_PCTRL2);
	hrouter_pctrl2 &= ~((AR8327_PORTS_ALL <<
				AR8327_HROUTER_PCTRL2_ARP_REQ_LEARN_S) |
				(AR8327_PORTS_ALL <<
				AR8327_HROUTER_PCTRL2_ARP_REP_LEARN_S));

	if (enable) {
		/* ARP ACK on CPU port needed to learn wifi hosts quickly
		 * e.g. when RaMP changes backbone from ethernet to wifi */
		frame_ack_ctrl0 |= 0x20200020;
		frame_ack_ctrl1 |= 0x202020;

		/* ARP_FORWARD_ACT=forward, otherwise ARP learning breaks
		 * ARP forwarding */
		fwd_ctrl0 |= (AR8327_FWD_CTRL0_ARP_FWD_ACT_FORWARD <<
				AR8327_FWD_CTRL0_ARP_FWD_ACT_S);

		/* otherwise learning takes too long */
		hrouter_ctrl |= AR8327_HROUTER_CTRL_ARP_OWR_EN |
				AR8327_HROUTER_CTRL_ARP_LEARN_MODE;

		/* learn from arp replies and requests on all ports */
		hrouter_pctrl2 |= (AR8327_PORTS_ALL <<
				AR8327_HROUTER_PCTRL2_ARP_REQ_LEARN_S) |
				(AR8327_PORTS_ALL <<
				AR8327_HROUTER_PCTRL2_ARP_REP_LEARN_S);
	}

	ar8xxx_write(priv, AR8327_REG_FRAME_ACK_CTRL0, frame_ack_ctrl0);
	ar8xxx_write(priv, AR8327_REG_FRAME_ACK_CTRL1, frame_ack_ctrl1);

	ar8xxx_write(priv, AR8327_REG_FWD_CTRL0, fwd_ctrl0);

	ar8xxx_write(priv, AR8327_L3_HROUTER_CTRL, hrouter_ctrl);
	ar8xxx_write(priv, AR8327_L3_HROUTER_PCTRL2, hrouter_pctrl2);
}

static void
ar8327_hnat_vlan_fixup(struct ar8xxx_priv *priv, bool enable)
{
	u32 i, v = 0;

	/* need to keep VLAN format after translation by ARP table
	 * HNAT logic */
	 /* mode 0: unmodified
	 *   Egress packet keep the final VLAN format after switch
	 *   VLAN translation process. It is different from mode 3
	 *   if we defined some VLAN translation process
	 * mode 3: untouch
	 *   Egress packet keeps the original format of ingress packets.
	 *   For example, ingress is untag, then egress is untag;
	 *   ingress is tagged, egress is untagged with same VID */

	/* read VLAN egress mode from WAN port and fill HNAT egress VLAN mode */
	if (enable)
		for (i = 0; i < AR8327_NUM_PORTS; i++)
			v |= (( ar8xxx_read(priv, AR8327_REG_PORT_VLAN1(1)) &
					AR8327_PORT_VLAN1_OUT_MODE) >>
					AR8327_PORT_VLAN1_OUT_MODE_S)
				<< AR8327_ROUTER_EG_VLAN_MODE_PORT(i);

	ar8xxx_write(priv, AR8327_REG_ROUTER_EG_VLAN_MODE, v);
}

static int
ar8327_l3_table_flush(struct ar8xxx_priv *priv, int table, int sp)
{
	u32 v = 0, reg = 0;
	bool sp_en = false;
	u32 data[4] = {0};
	int err = -1;

	switch (table) {
	case AR8327_L3_EECR_TABLE_NAPT:
		reg = AR8327_L3_NAPT_USED_COUNT;
		break;
	case AR8327_L3_EECR_TABLE_ARP:
		reg = AR8327_L3_ARP_USED_COUNT;
		break;
	}

	if (reg) {
		v = ar8xxx_read(priv, reg);
		if (!v)
			return 0;
	}

	if (sp >= 0) {
		sp_en = true;
		data[2] = (sp & 0x7) << 28;
		/* HW requirement: disable ARP learning */
		ar8327_arp_learn_en(priv, false);
	}

	/* flush table */
	err = ar8327_l3_table_write(priv, data, table,
				AR8327_L3_EECR_ENTRY_FUNC_FLUSH,
				(sp_en ? AR8327_L3_EECR_SPECIFIC_SP_EN : 0));

	if (sp_en) {
		/* restore ARP learning state */
		ar8327_arp_learn_en(priv, true);
	}

	return err;
}

static int
ar8327_acl_table_write(struct ar8xxx_priv *priv, u32 *data, u32 index)
{
	int sel;

	if (index >= AR8327_ACL_MAX_ENT)
		return -1;

	for (sel = AR8327_ACL_RULE_SEL_RULE;
		sel <= AR8327_ACL_RULE_SEL_RESULT;
		sel++)
	{
		ar8xxx_write(priv, AR8327_REG_ACL_FUNC1, data[sel*5 + 0]);
		ar8xxx_write(priv, AR8327_REG_ACL_FUNC2, data[sel*5 + 1]);
		ar8xxx_write(priv, AR8327_REG_ACL_FUNC3, data[sel*5 + 2]);
		ar8xxx_write(priv, AR8327_REG_ACL_FUNC4, data[sel*5 + 3]);
		ar8xxx_write(priv, AR8327_REG_ACL_FUNC5, data[sel*5 + 4]);

		ar8xxx_write(priv, AR8327_REG_ACL_FUNC0,
					AR8327_ACL_BUSY |
					AR8327_ACL_FUNC_WRITE |
					((sel << AR8327_ACL_RULE_SEL_S) &
						AR8327_ACL_RULE_SEL) |
					(index & AR8327_ACL_FUNC_INDEX));

		/* fail if hw is busy */
		if (ar8216_wait_bit(priv, AR8327_REG_ACL_FUNC0,
						AR8327_ACL_BUSY, 0))
			return -1;
	}

	return 0;
}

static int
ar8327_acl_table_get_next(struct ar8xxx_priv *priv, u32 *data, u32 index)
{
	int sel;

	for (sel = AR8327_ACL_RULE_SEL_RULE;
		sel <= AR8327_ACL_RULE_SEL_RESULT;
		sel++)
	{
		ar8xxx_write(priv, AR8327_REG_ACL_FUNC0,
					AR8327_ACL_BUSY |
					AR8327_ACL_FUNC_READ |
					((sel << AR8327_ACL_RULE_SEL_S) &
						AR8327_ACL_RULE_SEL) |
					(index & AR8327_ACL_FUNC_INDEX));

		/* fail if hw is busy */
		if (ar8216_wait_bit(priv, AR8327_REG_ACL_FUNC0,
						AR8327_ACL_BUSY, 0))
			return -1;

		data[sel*5 + 0] = ar8xxx_read(priv, AR8327_REG_ACL_FUNC1);
		data[sel*5 + 1] = ar8xxx_read(priv, AR8327_REG_ACL_FUNC2);
		data[sel*5 + 2] = ar8xxx_read(priv, AR8327_REG_ACL_FUNC3);
		data[sel*5 + 3] = ar8xxx_read(priv, AR8327_REG_ACL_FUNC4);
		data[sel*5 + 4] = ar8xxx_read(priv, AR8327_REG_ACL_FUNC5);

		/* test for invalid rule */
		if (sel == AR8327_ACL_RULE_SEL_RULE &&
					!(data[4] & AR8327_ACL_RULE_DATA4))
			return -1;
	}

	return 0;
}

static int
ar8327_hnat_war_init(struct ar8xxx_priv *priv)
{
	u32 data[15] = {0};

	if (ar8327_hnat_war_init_done)
		return 0;

	/* ACL rule */
	data[0] = 0xffffff00;
	data[1] = 0xffffff00;
	data[4] = 0x0000003d; /* physical source port P0,P2-P5 (LAN ports) */
	/* ACL mask */
	data[5] = 0xffffff00; /* DIP mask */
	data[6] = 0xffffff00; /* SIP mask */
	data[8] = 0x00030000; /* mask mode = mask (bytes 0:7) */
	data[9] = 0x000000c2; /* rule valid => start&end, type => IPv4 */
	/* ACL action: forward (0) */
	if (ar8327_acl_table_write(priv, data, 0))
		return -1;

	memset(data, 0, sizeof(data));

	/* ACL rule */
	data[1] = 0xffffff00;
	data[4] = 0x0000003d; /* physical source port P0,P2-P5 (LAN ports) */
	/* ACL mask */
	data[6] = 0xffffff00; /* SIP mask */
	data[8] = 0x00030000; /* mask mode = mask (bytes 0:7) */
	data[9] = 0x000000c2; /* rule valid => start&end, type => IPv4 */
	if (ar8327_acl_table_write(priv, data, 1))
		return -1;

	ar8327_hnat_war_init_done = 1;

	return 0;
}

static void
ar8327_hnat_war_update_lanip(struct ar8xxx_priv *priv, u32 ip)
{
	u32 data[15] = {0};

	ar8327_acl_table_get_next(priv, data, 0);
	data[0] = ip;
	data[1] = ip;
	ar8327_acl_table_write(priv, data, 0);

	memset(data, 0, sizeof(data));
	ar8327_acl_table_get_next(priv, data, 1);
	data[1] = ip;
	ar8327_acl_table_write(priv, data, 1);
}

static void
ar8327_hnat_war_update_idx(struct ar8xxx_priv *priv, u32 idx)
{
	u32 data[15] = {0};

	ar8327_acl_table_get_next(priv, data, 1);
	/* ACL action: force SNAT for selected ARP entry */
	data[11] = ((idx << AR8327_ACL_ACT_ARP_INDEX_S) &
				AR8327_ACL_ACT_ARP_INDEX) |
				AR8327_ACL_ACT_ARP_INDEX_OVER_EN |
	(AR8327_ACL_ACT_FORCE_L3_MODE_SNAT << AR8327_ACL_ACT_FORCE_L3_MODE_S);
	ar8327_acl_table_write(priv, data, 1);
}

static u32
ar8327_hnat_get_public_ip(struct ar8xxx_priv *priv)
{
	u32 v = ar8xxx_read(priv, 0x2100);
	if (ar8xxx_read(priv, 0x2a000) != v)
		return 0;
	if (ar8xxx_read(priv, 0x5aa00) != v)
		return 0;

	return v;
}

static bool
ar8327_hnat_public_ip_is_en(struct ar8xxx_priv *priv)
{
	if ((ar8xxx_read(priv, 0x2a040) & 1) != 1)
		return false;
	if ((ar8xxx_read(priv, 0x5aa04) & 1) != 1)
		return false;

	return true;
}

static void
ar8327_hnat_public_ip_en(struct ar8xxx_priv *priv)
{
	ar8xxx_write(priv, 0x2a040, 0x1); /* enable 0x2a000 entry */
	ar8xxx_write(priv, 0x5aa04, 0x1); /* enable 0x5aa00 entry */
}

static void
ar8327_hnat_set_public_ip(struct ar8xxx_priv *priv, u32 ip)
{
	ar8327_hnat_wan_ip = ip;
	ar8xxx_write(priv, 0x2100, ip);
	ar8xxx_write(priv, 0x2a000, ip); /* FIXME: 8337 address differs */
	ar8xxx_write(priv, 0x5aa00, ip);
	ar8327_hnat_public_ip_en(priv);
}

static u32
ar8327_hnat_get_priv_ip(struct ar8xxx_priv *priv)
{
	u32 v = ar8xxx_read(priv, AR8327_REG_PRIVATE_IP_CTRL);
	if (ar8xxx_read(priv, AR8327_L3_PRIV_BASE_IP_ADDR) == v)
		return v;

	return 0;
}

static void
ar8327_hnat_set_priv_ip(struct ar8xxx_priv *priv, u32 ip)
{
	ar8327_hnat_war_update_lanip(priv, ip);

	if (ar8327_hnat_get_priv_ip(priv) == (ip >> 12))
		return;

	ar8xxx_write(priv, AR8327_REG_PRIVATE_IP_CTRL, (ip >> 12));
	ar8xxx_write(priv, AR8327_L3_PRIV_BASE_IP_ADDR, (ip >> 12));
}

static void
ar8327_hnat_set_router_mac(struct ar8xxx_priv *priv, const u8 *mac,
				u8 idx, u16 vid)
{
	u32 v;

	v = mac[5] | (mac[4] << 8) | (mac[3] << 16) | (mac[2] << 24);
	ar8xxx_write(priv, 0x5a900 | (idx << 4), v);
	ar8xxx_write(priv, 0x2000 | (idx << 4), v);

	v = mac[1] | (mac[0] << 8);
	ar8xxx_write(priv, 0x5a904 | (idx << 4), v | ((vid & 0xfff) << 16) |
						((vid & 0xf) << 28));
	ar8xxx_write(priv, 0x2004 | (idx << 4), v | ((vid & 0xfff) << 16));

	/* ROUTER_MAC_ACT = 0x1 =IPv4 router */
	ar8xxx_write(priv, 0x5a908 | (idx << 4), 0x100 | ((vid & 0xfff) >> 4));
}

static void
ar8327_hnat_en(struct ar8xxx_priv *priv, bool enable)
{
	u32 v;

	/* TODO: for ar8337 check for rx_mangle function and WAN/LAN IP masks */
	if (!chip_is_ar8327(priv))
		return;

	priv->hnat_en = enable;

	v = ar8xxx_read(priv, AR8327_L3_HNAT_CTRL);
	v |= (AR8327_HNAT_CTRL_NAPT_MODE_PORT_STRICT <<
			AR8327_HNAT_CTRL_NAPT_MODE_S);
	if (enable)
		v |= (AR8327_HNAT_CTRL_HNAPT_EN | AR8327_HNAT_CTRL_HNAT_EN);
	else
		v &= ~(AR8327_HNAT_CTRL_HNAPT_EN | AR8327_HNAT_CTRL_HNAT_EN);
	ar8xxx_write(priv, AR8327_L3_HNAT_CTRL, v);

	v = ar8xxx_read(priv, AR8327_REG_MODULE_EN);
	if (enable)
		v |= AR8327_MODULE_EN_ACL | AR8327_MODULE_EN_L3;
	else
		v &= ~AR8327_MODULE_EN_L3;
	ar8xxx_write(priv, AR8327_REG_MODULE_EN, v);

	if (enable)
		v = AR8327_ATHEROS_HEADER |
			(AR8327_HEADER_CTRL_HEADER_LEN_4 <<
				AR8327_HEADER_CTRL_HEADER_LEN_S);
	else
		v = 0;
	ar8xxx_write(priv, AR8327_REG_HEADER_CTRL, v);

	printk("%s: %s CPU port tx header\n", __FUNCTION__,
			enable? "add" : "remove");
	if (enable)
		v = AR8327_PORT_HEADER_MODE_ALL;
	else
		v = 0;
	ar8xxx_write(priv, AR8327_REG_PORT_HEADER(AR8216_PORT_CPU), v);

	if (chip_is_ar8327(priv)) { /* different for ar8337 chip? */
		printk("%s:%sregistering eth_mangle_rx function\n",
			__FUNCTION__, enable? " " : " de" );
		priv->phy->attached_dev->phy_ptr = priv;
		if (enable)
			priv->phy->attached_dev->eth_mangle_rx = ar8327_mangle_rx;
		else
			priv->phy->attached_dev->eth_mangle_rx = NULL;
	}

	ar8327_hnat_public_ip_en(priv);
	ar8327_arp_learn_en(priv, enable);
	ar8327_hnat_vlan_fixup(priv, enable);
	ar8327_hnat_war_init(priv);
}

static int
ar8327_l3_table_get_next(struct ar8xxx_priv *priv, int table,
					u32 *data, u32 *index)
{
	/* get next entry */
	if (ar8327_l3_table_write(priv, NULL, table,
				AR8327_L3_EECR_ENTRY_FUNC_NEXT,
				((*index << AR8327_L3_EECR_CMD_INDEX_S)
					& AR8327_L3_EECR_CMD_INDEX)))
		return -1;

	/* found entry index */
	*index = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_CONTROL_REG);
	*index = (*index & AR8327_L3_EECR_CMD_INDEX) >>
			AR8327_L3_EECR_CMD_INDEX_S;

	data[0] = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_DATA0);
	data[1] = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_DATA1);
	data[2] = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_DATA2);
	data[3] = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_DATA3);

	return 0;
}

int
ar8327_sw_get_arp_table(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	char *buf = priv->l3_buf;
	u32 count, i;
	u32 data[4];
	int port, age = 0;
	int entrymax = AR8327_HNAT_ARP_MAX_ENT - 1;
	val->len = 0;

	mutex_lock(&priv->reg_mutex);

	count = ar8xxx_read(priv, AR8327_L3_ARP_USED_COUNT);
	if (!count) {
		val->len += snprintf(buf + val->len,
					sizeof(priv->l3_buf) - val->len,
					"ARP table empty\n");
		goto exit;
	}

	val->len += snprintf(buf + val->len, sizeof(priv->l3_buf) - val->len,
			"ARP table (size: %i)\n", count);

	i = entrymax;
	while (i < AR8327_HNAT_ARP_MAX_ENT) {
		if (ar8327_l3_table_get_next(priv, AR8327_L3_EECR_TABLE_ARP,
								data, &i))
			break;
		age = (data[3] >> 12) & 0x7;
		port = (data[2] >> 28) & 0x7;

		if (i == entrymax && !age)
			break;

		val->len += snprintf(buf + val->len,
					sizeof(priv->l3_buf) - val->len,
		"  %i: %i.%i.%i.%i = %02x:%02x:%02x:%02x:%02x:%02x"
		" at port %i, age: %i, idx: %i, cpu: %i\n",
			i,
			(data[0] >> 24) & 0xff, (data[0] >> 16) & 0xff,
			(data[0] >> 8) & 0xff, data[0] & 0xff,
			(data[2] >> 8) & 0xff, data[2] & 0xff,
			(data[1] >> 24) & 0xff, (data[1] >> 16) & 0xff,
			(data[1] >> 8) & 0xff, data[1] & 0xff,
			port, age, (data[2] >> 16) & 7, (data[2] >> 31));

		if (i == entrymax)
			break;
	}

exit:
	val->value.s = buf;

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_flush_arp_table(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	int ret;

	mutex_lock(&priv->reg_mutex);
	ret = ar8327_l3_table_flush(priv, AR8327_L3_EECR_TABLE_ARP, -1);
	mutex_unlock(&priv->reg_mutex);

	return ret;
}

int
ar8327_sw_get_acl_table(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	char *buf = priv->l3_buf;
	u32 i = 0;
	u32 data[15];
	val->len = 0;

	mutex_lock(&priv->reg_mutex);

	val->len += snprintf(buf + val->len,
				sizeof(priv->l3_buf) - val->len,
				"ACL table:\n");

	for (i = 0; i < AR8327_ACL_MAX_ENT; i++) {
		if (ar8327_acl_table_get_next(priv, data, i))
			continue;

		val->len += snprintf(buf + val->len,
					sizeof(priv->l3_buf) - val->len,
		"  %i:\n"
		"    rule: 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n"
		"    mask: 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n"
		"    act:  0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n",
			i,
			data[0], data[1], data[2], data[3], data[4],
			data[5], data[6], data[7], data[8], data[9],
			data[10], data[11], data[12], data[13], data[14]);
	}

	val->value.s = buf;

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_get_napt_table(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	char *buf = priv->l3_buf;
	size_t buf_size = sizeof(priv->l3_buf);
	size_t buf_size_left;
	u32 count, i;
	u32 data[4];
	int age = 0;
	int entrymax = AR8327_HNAT_NAPT_MAX_ENT - 1;
	int ret, len = 0;

	mutex_lock(&priv->reg_mutex);

	count = ar8xxx_read(priv, AR8327_L3_NAPT_USED_COUNT);
	if (!count) {
		len += snprintf(buf + len, buf_size - len,
					"NAPT table empty\n");
		goto exit;
	}

	len += snprintf(buf + len, buf_size - len,
			"NAPT table (size: %i)\n", count);

	i = entrymax;
	while (i < AR8327_HNAT_NAPT_MAX_ENT) {
		if (ar8327_l3_table_get_next(priv, AR8327_L3_EECR_TABLE_NAPT,
								data, &i))
			break;
		age = (data[3] >> 12) & 0xf;

		if (i == entrymax && !age)
			break;

		buf_size_left = buf_size - len;
		ret = snprintf(buf + len, buf_size_left,
		"  %i: DIP = %i.%i.%i.%i, DP = %i, SP = %i,"
		" XP = %i, XIP idx = %i, SIP = 0x%03x\n"
		"        act: %i, proto: %i, age: %i\n",
			i,
			(data[0] >> 24) & 0xff, (data[0] >> 16) & 0xff,
			(data[0] >> 8) & 0xff, data[0] & 0xff,
			data[1] & 0xffff, (data[1] >> 16) & 0xffff,
			data[2] & 0xffff, (data[2] >> 16) & 0xf,
			(data[2] >> 20) & 0xfff, data[3] & 0x3,
			(data[3] >> 2) & 0x3, age);

		if (ret >= 0 && ret < buf_size_left)
			len += ret;
		else
			break;

		if (i == entrymax)
			break;
	}

exit:
	val->value.s = buf;
	val->len = len;

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_flush_napt_table(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	int ret;

	mutex_lock(&priv->reg_mutex);
	ret = ar8327_l3_table_flush(priv, AR8327_L3_EECR_TABLE_NAPT, -1);
	mutex_unlock(&priv->reg_mutex);

	return ret;
}

int
ar8327_sw_get_hnat_wanip(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	char *buf = priv->l3_buf;
	u32 v;
	bool en;

	mutex_lock(&priv->reg_mutex);
	v = ar8327_hnat_get_public_ip(priv);
	en = ar8327_hnat_public_ip_is_en(priv);
	mutex_unlock(&priv->reg_mutex);

	val->len = snprintf(buf, sizeof(priv->l3_buf), "%i.%i.%i.%i (%i)",
				(v >> 24) & 0xff, (v >> 16) & 0xff,
				(v >> 8) & 0xff, v & 0xff, en);
	val->value.s = buf;

	return 0;
}

int
ar8327_sw_set_hnat_wanip(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u32 ip1, ip2, ip3, ip4;

	if (sscanf(val->value.s, "%u.%u.%u.%u",
			&ip1, &ip2, &ip3, &ip4) != 4)
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	ar8327_hnat_set_public_ip(priv, ((ip1 & 0xff) << 24) |
					((ip2 & 0xff) << 16) |
					((ip3 & 0xff) << 8) |
					(ip4 & 0xff));
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_get_hnat_lanip(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	char *buf = priv->l3_buf;
	u32 v;

	mutex_lock(&priv->reg_mutex);
	v = ar8327_hnat_get_priv_ip(priv);
	mutex_unlock(&priv->reg_mutex);

	val->len = snprintf(buf, sizeof(priv->l3_buf), "%i.%i.%i",
				(v >> 12) & 0xff, (v >> 4) & 0xff, v & 0xf);
	val->value.s = buf;

	return 0;
}

int
ar8327_sw_set_hnat_lanip(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u32 ip1, ip2, ip3, ip4;

	if (sscanf(val->value.s, "%u.%u.%u.%u",
			&ip1, &ip2, &ip3, &ip4) != 4)
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	ar8327_hnat_set_priv_ip(priv, ((ip1 & 0xff) << 24) |
					((ip2 & 0xff) << 16) |
					((ip3 & 0xff) << 8) |
					(ip4 & 0xff));
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_get_hnat_mac(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	char *buf = priv->l3_buf;
	u32 v, idx;

	mutex_lock(&priv->reg_mutex);

	val->len = 0;

	for (idx = 0; idx < AR8327_MAC_MAX_ENT; idx++) {
		v = ar8xxx_read(priv, 0x2004 | (idx << 4));
		val->len += snprintf(buf + val->len,
				sizeof(priv->l3_buf) - val->len,
				"%02x:%02x:", (v >> 8) & 0xff, v & 0xff);

		v = ar8xxx_read(priv, 0x2000 | (idx << 4));
		val->len += snprintf(buf + val->len,
					sizeof(priv->l3_buf) - val->len,
					"%02x:%02x:%02x:%02x\n",
					(v >> 24) & 0xff, (v >> 16) & 0xff,
					(v >> 8) & 0xff, v & 0xff);
	}

	val->value.s = buf;

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_hnat_mac_lan(struct switch_dev *dev,
				const struct switch_attr *attr,
				struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u8 mac[6];

	if (sscanf(val->value.s, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
				&mac[0], &mac[1], &mac[2],
				&mac[3], &mac[4], &mac[5]) != 6)
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	ar8327_hnat_set_router_mac(priv, mac, ar8327_hnat_mac_idx_lan,
						ar8327_hnat_mac_vid_lan);
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_get_hnat_mac_wan_vid(struct switch_dev *dev,
				const struct switch_attr *attr,
				struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);

	mutex_lock(&priv->reg_mutex);
	val->value.i = ar8327_hnat_mac_vid_wan;
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_hnat_mac_wan_vid(struct switch_dev *dev,
				const struct switch_attr *attr,
				struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);

	/* VID 1 is always LAN */
	if (!(val->value.i > 1 && val->value.i < 4096))
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	ar8327_hnat_mac_vid_wan = val->value.i;
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_hnat_mac_wan(struct switch_dev *dev,
				const struct switch_attr *attr,
				struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u8 mac[6];

	if (sscanf(val->value.s, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
				&mac[0], &mac[1], &mac[2],
				&mac[3], &mac[4], &mac[5]) != 6)
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	/* TODO: need also set VID in 0x0c70 ? */
	ar8327_hnat_set_router_mac(priv, mac, ar8327_hnat_mac_idx_wan,
						ar8327_hnat_mac_vid_wan);
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

static int
ar8327_hnat_arp(struct ar8xxx_priv *priv,
				u32 ip, u8 *mac,
				u8 port, u8 rmac_idx, u8 func)
{
	int idx = -1;
	u32 data[4] = {0};

	data[0] = ip;
	data[1] = (mac[2] << 24) | (mac[3] << 16) | (mac[4] << 8) | mac[5];
	data[2] = (port? 0 : (1 << 31)) |
		(port << 28) |
		(rmac_idx << 16) |
		(mac[0] << 8) |
		mac[1];
	/* static entry, action=forward */
	data[3] = 0x00007000 | 0x3;

	if (ar8327_l3_table_write(priv, data,
					AR8327_L3_EECR_TABLE_ARP,
					func, 0))
		return -1;

	idx = ar8xxx_read(priv, AR8327_L3_ENTRY_EDIT_CONTROL_REG);
	idx = (idx & AR8327_L3_EECR_CMD_INDEX) >>
			AR8327_L3_EECR_CMD_INDEX_S;

	return idx;
}

static int
ar8327_hnat_arp_add(struct ar8xxx_priv *priv,
				u32 ip, u8 *mac,
				u8 port, u8 rmac_idx)
{
	return ar8327_hnat_arp(priv, ip, mac, port, rmac_idx,
				AR8327_L3_EECR_ENTRY_FUNC_ADD);
}

static int
ar8327_hnat_arp_del(struct ar8xxx_priv *priv,
				u32 ip, u8 *mac,
				u8 port, u8 rmac_idx)
{
	return ar8327_hnat_arp(priv, ip, mac, port, rmac_idx,
				AR8327_L3_EECR_ENTRY_FUNC_DEL);
}

int
ar8327_sw_set_hnat_gw(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u8 mac[6];
	u32 ip1, ip2, ip3, ip4, ip, idx;

	if (sscanf(val->value.s,
			"%u.%u.%u.%u=%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
			&ip1, &ip2, &ip3, &ip4,
			&mac[0], &mac[1], &mac[2],
			&mac[3], &mac[4], &mac[5]) != 10)
		return -EINVAL;

	ip = ((ip1 & 0xff) << 24) |
		((ip2 & 0xff) << 16) |
		((ip3 & 0xff) << 8) |
		(ip4 & 0xff);

	mutex_lock(&priv->reg_mutex);

	if (ar8327_hnat_is_en(priv))
		ar8327_arp_learn_en(priv, false);

	/* FIXME: after flushing specific port entries, HNAT engine goes into
	 * limbo state and outgoing packets are not accelerated :(
	 * instead of flushing all or specific port entries we
	 * just delete a specific DIP from the table */
	if (ar8327_hnat_arp_gw_ip)
		ar8327_hnat_arp_del(priv, ar8327_hnat_arp_gw_ip,
					ar8327_hnat_arp_gw_mac, 1, 1);

	memcpy(ar8327_hnat_arp_gw_mac, mac, sizeof(ar8327_hnat_arp_gw_mac));
	ar8327_hnat_arp_gw_ip = ip;

	/* add/make static WAN gw ARP entry */
	/* WAN gw is on port 1 always, router mac index = 1 */
	idx = ar8327_hnat_arp_add(priv, ip, mac, 1, 1);

	if (ar8327_hnat_is_en(priv))
		ar8327_arp_learn_en(priv, true);

	ar8327_hnat_war_update_idx(priv, idx);

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_hnat_arp_lan(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u8 mac[6];
	u32 ip1, ip2, ip3, ip4, ip;

	if (sscanf(val->value.s,
			"%u.%u.%u.%u=%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
			&ip1, &ip2, &ip3, &ip4,
			&mac[0], &mac[1], &mac[2],
			&mac[3], &mac[4], &mac[5]) != 10)
		return -EINVAL;

	ip = ((ip1 & 0xff) << 24) |
		((ip2 & 0xff) << 16) |
		((ip3 & 0xff) << 8) |
		(ip4 & 0xff);

	mutex_lock(&priv->reg_mutex);

	if (ar8327_hnat_is_en(priv))
		ar8327_arp_learn_en(priv, false);

	if (ar8327_hnat_arp_lan_ip)
		ar8327_hnat_arp_del(priv, ar8327_hnat_arp_lan_ip,
					ar8327_hnat_arp_lan_mac, 0, 0);

	memcpy(ar8327_hnat_arp_lan_mac, mac, sizeof(ar8327_hnat_arp_lan_mac));
	ar8327_hnat_arp_lan_ip = ip;

	ar8327_hnat_arp_add(priv, ip, mac, 0, 0);

	if (ar8327_hnat_is_en(priv))
		ar8327_arp_learn_en(priv, true);

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_hnat_arp_wan(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	u8 mac[6];
	u32 ip1, ip2, ip3, ip4, ip;

	if (sscanf(val->value.s,
			"%u.%u.%u.%u=%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
			&ip1, &ip2, &ip3, &ip4,
			&mac[0], &mac[1], &mac[2],
			&mac[3], &mac[4], &mac[5]) != 10)
		return -EINVAL;

	ip = ((ip1 & 0xff) << 24) |
		((ip2 & 0xff) << 16) |
		((ip3 & 0xff) << 8) |
		(ip4 & 0xff);

	mutex_lock(&priv->reg_mutex);

	if (ar8327_hnat_is_en(priv))
		ar8327_arp_learn_en(priv, false);

	if (ar8327_hnat_arp_wan_ip)
		ar8327_hnat_arp_del(priv, ar8327_hnat_arp_wan_ip,
					ar8327_hnat_arp_wan_mac, 0, 1);

	memcpy(ar8327_hnat_arp_wan_mac, mac, sizeof(ar8327_hnat_arp_wan_mac));
	ar8327_hnat_arp_wan_ip = ip;

	ar8327_hnat_arp_add(priv, ip, mac, 0, 1);

	if (ar8327_hnat_is_en(priv))
		ar8327_arp_learn_en(priv, true);

	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_get_hnat(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);

	mutex_lock(&priv->reg_mutex);
	val->value.i = ar8327_hnat_is_en(priv);
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_hnat(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);

	if (val->value.i != 0 && val->value.i != 1)
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	ar8327_hnat_en(priv, !!val->value.i);
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_arp_learn(struct switch_dev *dev,
			const struct switch_attr *attr,
			struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);

	if (val->value.i != 0 && val->value.i != 1)
		return -EINVAL;

	mutex_lock(&priv->reg_mutex);
	ar8327_arp_learn_en(priv, !!val->value.i);
	mutex_unlock(&priv->reg_mutex);

	return 0;
}

int
ar8327_sw_set_flush_arp_port(struct switch_dev *dev,
				const struct switch_attr *attr,
				struct switch_val *val)
{
	struct ar8xxx_priv *priv = swdev_to_ar8xxx(dev);
	int ret = 0;

	mutex_lock(&priv->reg_mutex);
	if (ar8327_l3_table_flush(priv, AR8327_L3_EECR_TABLE_ARP, val->value.i))
		ret = -1;
	mutex_unlock(&priv->reg_mutex);

	return ret;
}
