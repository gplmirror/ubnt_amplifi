#include "blocker.h"
#include "dnsmasq.h"
#include <net/ethernet.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define MAX_NAME_SIZE 253
#define MAX_LEVEL_CNT 127

#define ERROR(fmt, ...) my_syslog(LOG_ERR, _(fmt), __VA_ARGS__)
#define WARNING(fmt, ...) my_syslog(LOG_WARNING, _(fmt), __VA_ARGS__)

/* ========= BLACKLIST ========= */
#define name_eq(name1, name2) hostname_isequal(name1, name2)
#define name_in(name1, name2) hostname_contains(name1, name2)

/*djb2 see http://www.cse.yorku.ca/~oz/hash.html*/
static size_t hash(const char *name, size_t limit)
{
	size_t h = 5381, i = 0, size = strlen(name);
	unsigned char c;
	for (i = 0; i < size; ++i)
	{
		c = name[i];
		if (c >= 'A' && c <= 'Z')
			c += 'a' - 'A';
		h = (h << 5) + h + c;
	}
	return h & (limit - 1);
}

static size_t domain_deeps(const char* name)
{
	size_t cnt = 0;
	while (*name)
	{
		if (*name == '.')
			++cnt;
		++name;
	}
	return ++cnt;
}

static const char *subdomain(const char* name, unsigned char level)
{
	const char *p = name;
	unsigned char l = 0;

	if (!level) return name;
	while (*p) ++p;
	while (p != name)
		if (*--p == '.')
			if (++l == level)
				return ++p;
	if (l + 1 == level)
		return name;
	return NULL;
}

static int abort_blacklist_load(const char *msg, const char *filename, int fd)
{
	ERROR("failed to load blacklist \"%s\": %s: %s", filename, msg, strerror(errno));
	if (fd >= 0)
		close(fd);
	return 0;
}

static char* next_name(char *name, char *name_end)
{
    while (name != name_end && !*name)
        name++;
    return name;
}

int blacklist_load(struct blacklist *bl, const char *filename, const char *blacklistname)
{
	struct blacklist_node free_nodes, *s, *e;
	char *name, *name_end;
	size_t name_len = 0;
	size_t node_pos = 0;
	size_t substrs_cnt = 0, substrs_pos = 0;
	size_t hast_size = 0;
	size_t i = 0;
	unsigned char level[MAX_LEVEL_CNT + 1] = {0};
	size_t deeps, level_pos = 0;
    int fd;
    struct stat st = {};
    char is_comment = 0;
	memset(bl, 0, sizeof(struct blacklist));
	if (blacklistname)
		bl->name = strdup(blacklistname);
	else
		bl->name = strdup(filename);
	/* read blacklist */
    fd = open(filename, O_RDWR);
    if(fd < 0)
       return abort_blacklist_load("Cannot open blacklist", filename, 0);

    if (fstat(fd, &st))
       return abort_blacklist_load("Cannot get file size", filename, fd);
    bl->addr_size = st.st_size;

    if (bl->addr_size && (bl->addr = mmap(NULL, bl->addr_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED)
       return abort_blacklist_load("Cannot map blacklist to memory", filename, fd);

    /* change whitespaces and comments to nulls */
    while (i < bl->addr_size) {
        if (!is_comment && bl->addr[i] == '#')
            is_comment = 1;

        if (is_comment || isspace(bl->addr[i])) {
            if (bl->addr[i] == '\n')
                is_comment = 0;
            bl->addr[i] = 0;
        }
        ++i;
    }

    close(fd);

    name_end = bl->addr + bl->addr_size;
    name = next_name(bl->addr, name_end);

    while (name != name_end) {
        name_len = strlen(name);

        deeps = domain_deeps(name);
        if (name_len > MAX_NAME_SIZE || (name_len == 1 && strchr(":.", name[0])) || deeps > MAX_LEVEL_CNT)
        {
            WARNING("Bad name \"%s\" in blacklist \"%s\"", name, filename);
            continue;
        }

        if (':' == name[0])
            substrs_cnt++;
        else
        {
            if ('.' == name[0])
                level[deeps-1] = 1;
            else
                level[0] = 1;
            bl->size += 1;
        }
        name = next_name(name + name_len, name_end);
    }

	if (!bl->size && !substrs_cnt)
	{
		WARNING("Nothing to load from %s", filename);
		return 1;
	}

	/* fill hash table and substr list */
	if (bl->size)
	{
		for (hast_size = 1; hast_size < bl->size; hast_size<<=1); /* we won't use modulus */
		bl->size = hast_size;
		bl->nodes = (struct blacklist_node*)calloc(bl->size, sizeof(struct blacklist_node));
		if (!bl->nodes)
			return abort_blacklist_load("Cannot alloc mem for hash table", filename, 0);
		INIT_LIST_HEAD(&free_nodes.list);
		for(node_pos = 0; node_pos < bl->size; ++node_pos)
			list_add_tail(&bl->nodes[node_pos].list, &free_nodes.list);

		for (deeps = 0; deeps < MAX_LEVEL_CNT + 1; ++deeps)
			if (level[deeps])
				++bl->level_count;
		bl->level = (unsigned char*)calloc(bl->level_count, sizeof(unsigned char));
		if (!bl->level)
			return abort_blacklist_load("Cannot alloc mem for level list", filename, 0);
		for (deeps = 0; deeps < MAX_LEVEL_CNT + 1; ++deeps)
			if (level[deeps])
				bl->level[level_pos++] = deeps;
	}
	if (substrs_cnt)
	{
		bl->substrs = (char**)calloc(substrs_cnt + 1, sizeof(char*));
		if (!bl->substrs)
			return abort_blacklist_load("Cannot alloc mem for substr list", filename, 0);
		bl->substrs[substrs_cnt] = NULL;
	}

    name = next_name(bl->addr, name_end);
	do
	{
		if (':' == name[0]) /* sub-strings */
		{
			bl->substrs[substrs_pos++] = &name[1];
		}
		else /* hash table */
		{
			if ('.' == name[0]) ++name;

			s = &bl->nodes[hash(name, bl->size)];
			if (!s->name)
			{
				s->is_slot = 1;
				s->name = name;
				list_del_init(&s->list);
			}
			else if (s->is_slot)
			{
				e = list_first_entry(&free_nodes.list, struct blacklist_node, list);
				e->name = name;
				list_move_tail(&e->list, &s->list);
			}
			else
			{
				e = list_first_entry(&free_nodes.list, struct blacklist_node, list);
				e->name = s->name;
				list_move_tail(&e->list, &s->list);
				s->is_slot = 1;
				s->name = name;
				list_del_init(&s->list);
			}
		}
        name = next_name(name + strlen(name), name_end);
	} while (name != name_end);

	return 1;
}

int blacklist_has(struct blacklist *bl, const char *name)
{
	struct blacklist_node *s, *e;
	char ** substr;
	const char *subname = name;
	unsigned char i;
	if (!bl || !name)
		return 0;
	/* search sub-strings*/
	if (bl->substrs)
		for (substr = bl->substrs; *substr; ++substr)
			if (name_in(name, *substr))
				return 1;
	/* search name in hash table */
	for (i = 0; i < bl->level_count; ++i)
	{
		subname = subdomain(name, bl->level[i]);

        if (!subname)
           break;

		s = &bl->nodes[hash(subname, bl->size)];
		if (!s->is_slot)
			continue;
		if (name_eq(s->name, subname) && ((s->name > bl->addr && s->name[-1] == '.') || name == subname))
			return 1;
		list_for_each_entry(e, &s->list, list)
			if (name_eq(e->name, subname) && ((e->name > bl->addr && e->name[-1] == '.') || name == subname))
				return 1;
	}
	return 0;
}

void blacklist_unload(struct blacklist *bl)
{
	free(bl->substrs);
	free(bl->level);
	free(bl->nodes);
	free(bl->name);
    if (bl->addr_size && bl->addr > 0)
        munmap(bl->addr, bl->addr_size);
	memset(bl, 0, sizeof(struct blacklist));
}

/* ========= TIMEGAP ========= */
#define h24 86400 /* 24h in sec */
#define d7 127 /* all days of week - 01111111 */

int timegap_init(struct timegap *tg, const char *timer)
{
	unsigned int h_from = 0, m_from = 0, s_from = 0, h_to = 0, m_to = 0, s_to = 0, wd;
	int rc;
	const char *p = timer;
	memset(tg, 0, sizeof(struct timegap));
	tg->stop_time = h24;

	if (!strlen(timer))
		return 1;

	rc = sscanf(timer, "%u:%u:%u|%u:%u:%u|%u", &h_from, &m_from, &s_from, &h_to, &m_to, &s_to, &wd);
	if (rc < 1)
	{
		ERROR("Can't load timegap \"%s\": Input data is garbage.", timer);
		return 0;
	}

	/* star time */
	if (rc > 2)
	{
		if (h_from > 23 || m_from > 59 || s_from > 59)
		{
			ERROR("Can't load timegap \"%s\": Invalid time format [FROM].", timer);
			return 0;
		}
		tg->start_time = h_from*60*60 + m_from*60 + s_from;
	}

	/* stop time */
	if (rc > 5)
	{
		if (h_to > 23 || m_to > 59 || s_to > 59)
		{
			ERROR("Can't load timegap \"%s\": Invalid time format [TO].", timer);
			return 0;
		}
		tg->stop_time = h_to*60*60 + m_to*60 + s_to;
	}

	/* week days */
	if (rc == 1 || rc == 4 || rc == 7)
	{
		if (rc >= 4)
			while (p && *p++ != '|');
		if (rc == 7)
			while (p && *p++ != '|');
		while (*p)
		{
			if (!isdigit(*p))
			{
				ERROR("Can't load timegap \"%s\": Invalid symbol of DoW.", timer);
				return 0;
			}
			wd = p[0] - '0';
			if (wd > 7 )
			{
				ERROR("Can't load timegap \"%s\": DoW out of range [0:7].", timer);
				return 0;
			}
			if (wd == 7) wd = 0;
			tg->week_days |= 1 << wd;
			++p;
		}
	}

	return 1;
}

int timegap_has(struct timegap *tg, struct tm *time_point)
{
	 int tpoint;
	 /* check week day */
	 if (tg->week_days && ~tg->week_days & 1 << time_point->tm_wday)
		 return 0;

	 /* check time */
	 if (tg->start_time == 0 && tg->stop_time == h24)
		 return 1;
	 tpoint = time_point->tm_hour*60*60 + time_point->tm_min*60 + time_point->tm_sec;
	 if (tg->start_time <= tg->stop_time && tg->start_time <= tpoint && tg->stop_time >= tpoint)
		 return 1;
	 if (tg->start_time > tg->stop_time && (tg->start_time <= tpoint || tg->stop_time >= tpoint))
		 return 1;

	 return 0;
}

int timegap_includes(struct timegap *tg, struct timegap *other)
{
	const int wod_this = tg->week_days ? tg->week_days : d7;
	const int wod_other = other->week_days ? other->week_days : d7;

	if ((wod_this & wod_other) != wod_other)
		return 0;

	if (tg->start_time == 0 && tg->stop_time == h24)
		return 1;

	if ((tg->stop_time - tg->start_time) * (other->stop_time - other->start_time) > 0)
	{
		if (tg->start_time <= other->start_time && other->stop_time <= tg->stop_time)
			return 1;
	}
	else if (other->start_time <= other->stop_time)
	{
		if ((0 <= other->start_time && other->stop_time <= tg->stop_time) ||
			(tg->start_time <= other->start_time && other->stop_time <= h24))
				return 1;
	}

	return 0;
}

/* ========= TIMEGAPS ========= */
void timegaps_init(struct timegaps *tgs)
{
	memset(tgs, 0, sizeof(struct timegaps));
	INIT_LIST_HEAD(&tgs->list);
}

int timegaps_add(struct timegaps *tgs, struct timegap *tg)
{
	struct timegaps *tgap, *tmp;
	struct timegaps *new_tgap = malloc(sizeof(struct timegaps));
	if (!new_tgap)
	{
		ERROR("%s", "Cannot alloc memmory for timegap");
		return 0;
	}
	new_tgap->tgap = *tg;
	/* check if there is gap overlapping */
	list_for_each_entry_safe(tgap, tmp, &tgs->list, list)
		if (timegap_includes(&tgap->tgap, &new_tgap->tgap))
		{
			free(new_tgap);
			return 1;
		}
		else if (timegap_includes(&new_tgap->tgap, &tgap->tgap))
		{
			list_del(&tgap->list);
			free(tgap);
		}
	/* add gap to lists  */
	list_add(&new_tgap->list, &tgs->list);
	return 1;
}

int timegaps_has(struct timegaps *tgs, struct tm *time_point)
{
	struct timegaps *tgap, *tmp;
	list_for_each_entry_safe(tgap, tmp, &tgs->list, list)
		if (timegap_has(&tgap->tgap, time_point))
			return 1;
	return 0;
}

void timegaps_fini(struct timegaps *tgs)
{
	struct timegaps *tgap, *tmp;

	list_for_each_entry_safe(tgap, tmp, &tgs->list, list)
	{
		list_del(&tgap->list);
		free(tgap);
	}
}

/* ========= BLOCKEE ========= */
int blockee_init(struct blockee *be, char *id_str)
{
	if (!id_str)
	{
		memset(be->addr, 0, ETHER_ADDR_LEN);
		return 1;
	}

	if (ETHER_ADDR_LEN == sscanf(id_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
					&be->addr[0], &be->addr[1], &be->addr[2], &be->addr[3], &be->addr[4], &be->addr[5]))
		return 1;

	return 0;
}

int blockee_equal(struct blockee *be, struct blockee *other)
{
	return 0 == memcmp(be->addr, other->addr, ETHER_ADDR_LEN) ? 1 : 0;
}

int blockee_is_default(struct blockee *be)
{
	static struct blockee blockee_default;
	static int init = 0;
	if (!init)
	{
		blockee_init(&blockee_default, NULL);
		init = 1;
	}

	return blockee_equal(be, &blockee_default);
}

/* ========= BLOCKER ========= */
static struct blocker *blocker_create_user(struct blocker *br, struct blockee *user)
{
	struct blocker *b_user;
	/* Check is users already exist */
	list_for_each_entry(b_user, &br->list, list)
		if (blockee_equal(&b_user->id, user))
			return b_user;

	/* Create new user if missing */
	b_user = calloc(1, sizeof(struct blocker));
	if (!b_user)
	{
		ERROR("%s", "Cannot alloc mem to create new user");
		return NULL;
	}
	memcpy(&b_user->id, user, sizeof(struct blockee));
	INIT_LIST_HEAD(&b_user->bl.list);
	INIT_LIST_HEAD(&b_user->wl.list);
	if (blockee_is_default(user))
		list_add(&b_user->list, &br->list);
	else
		list_add_tail(&b_user->list, &br->list);
	return b_user;
}

void blocker_init(struct blocker *br)
{
	memset(br, 0, sizeof(struct blocker));
	INIT_LIST_HEAD(&br->bl.list);
	INIT_LIST_HEAD(&br->wl.list);
	INIT_LIST_HEAD(&br->list);
}

int blocker_load_list(struct blocker *br, const char *filename, const char *listname)
{
	struct blacklist *bl;
	struct blocker_list *b_list;
	/* Update/reload blacklist if it exist */
	list_for_each_entry(b_list, &br->bl.list, list)
		if (!strcmp(listname, b_list->bl->name))
		{
			blacklist_unload(b_list->bl);
			if (!blacklist_load(b_list->bl, filename, listname))
			{
				ERROR("Cannot reload blacklist \"%s\"", filename);
				blacklist_unload(b_list->bl);
				return 0;
			}
			return 1;
		}
	/* Added new blacklist if missing */
	bl = malloc(sizeof(struct blacklist));
	b_list = malloc(sizeof(struct blocker_list));
	if (!bl || !b_list)
	{
		free(bl);
		free(b_list);
		ERROR("Cannot alloc mem to create new blacklist \"%s\"", filename);
		return 0;
	}
	if (!blacklist_load(bl, filename, listname))
	{
		ERROR("Cannot load blacklist \"%s\"", filename);
		blacklist_unload(bl);
		free(bl);
		free(b_list);
		return 0;
	}
	b_list->bl = bl;
	list_add(&b_list->list, &br->bl.list);
	return 1;
}

int blocker_bind_user(struct blocker *br, struct blockee *user, struct timegap *timer, const char *listname, char exclude)
{
	struct blocker *b_user;
	struct blocker_list *b_list;
	struct blacklist *bl = NULL;

	/* Create new user or return existing one */
	b_user = blocker_create_user(br, user);
	if (!b_user)
		return 0;
	/* Find blacklist */
	list_for_each_entry(b_list, &br->bl.list, list)
		if (!strcmp(listname, b_list->bl->name))
		{
			bl = b_list->bl;
			break;
		}
	if (!bl)
	{
		ERROR("Unkown blacklist: \"%s\"", listname);
		return 0;
	}
	/* Update blacklist/whitelist if user already has this one */
	list_for_each_entry(b_list, exclude ? &b_user->wl.list : &b_user->bl.list, list)
		if (!strcmp(listname, b_list->bl->name))
		{
			b_list->bl = bl;
			return timegaps_add(&b_list->tgaps, timer);
		}
	/* Added blacklist/whitelist to user if missing */
	b_list = calloc(1, sizeof(struct blocker_list));
	if (!b_list)
	{
		ERROR("%s","Cannot alloc mem to add user to blacklist");
		return 0;
	}
	list_add(&b_list->list, exclude ? &b_user->wl.list : &b_user->bl.list);
	b_list->bl = bl;
	timegaps_init(&b_list->tgaps);
	return timegaps_add(&b_list->tgaps, timer);
}

void blocker_unbind_users(struct blocker *br)
{
	struct blocker *b_user, *tmp1;
	struct blocker_list *b_list, *b_list_head, *tmp2;
	list_for_each_entry_safe(b_user, tmp1, &br->list, list)
	{
		list_del(&b_user->list);
		for (b_list_head = &b_user->bl;
				b_list_head;
				b_list_head = b_list_head == &b_user->bl ? &b_user->wl : NULL)
			list_for_each_entry_safe(b_list, tmp2, &b_list_head->list, list)
			{
				list_del(&b_list->list);
				timegaps_fini(&b_list->tgaps);
				free(b_list);
			}
		free(b_user);
	}
}

static struct tm * get_current_time()
{
	time_t curr_time;
	time(&curr_time);
	return localtime(&curr_time);
}

int blocker_has(struct blocker *br, struct blockee *user, const char *name)
{
	struct blocker *b_user = NULL, *b_default_user = NULL;
	struct blocker_list *b_list, *b_default_list;
	char exclude = 0;
	struct tm *curr_time = get_current_time();

	if (list_empty(&br->list))
		return 0;

	/* Find user */
	list_for_each_entry(b_user, &br->list, list)
		if (blockee_equal(&b_user->id, user))
			break;

	/* First check if name is in default blacklists */
	b_default_user = list_first_entry(&br->list, struct blocker, list);
	if (blockee_is_default(&b_default_user->id))
		list_for_each_entry(b_default_list, &b_default_user->bl.list, list)
			if (timegaps_has(&b_default_list->tgaps, curr_time) &&
				blacklist_has(b_default_list->bl, name))
			{
				exclude = 0;
				if (b_user != br)
					list_for_each_entry(b_list, &b_user->wl.list, list)
						if (timegaps_has(&b_list->tgaps, curr_time) &&
							!strcmp(b_list->bl->name, b_default_list->bl->name))
						{
							exclude = 1;
							break;
						}
				/* Skip if it is user specific exception */
				if (!exclude)
					return 1;
			}

	/* Then check if name is in users specific blacklists */
	if (b_user != br)
		list_for_each_entry(b_list, &b_user->bl.list, list)
			if (timegaps_has(&b_list->tgaps, curr_time) && blacklist_has(b_list->bl, name))
				return 1;
	return 0;
}

void blocker_fini(struct blocker *br)
{
	struct blocker_list *b_list, *tmp;

	blocker_unbind_users(br);

	list_for_each_entry_safe(b_list, tmp, &br->bl.list, list)
	{
		list_del(&b_list->list);
		blacklist_unload(b_list->bl);
		free(b_list->bl);
		free(b_list);
	}
}

void blocker_reinit(struct blocker *br)
{
	blocker_fini(br);
	blocker_init(br);
}
