#ifndef BLOCKER_H_
#define BLOCKER_H_

#include <libubox/list.h>
#include <time.h>

/* ========= BLACKLIST ========= */
struct blacklist_node
{
	char is_slot;
	char *name;
	struct list_head list;
};

struct blacklist
{
	char *name;
	char **substrs;
	struct blacklist_node *nodes;
	size_t size;
	unsigned char *level;
	unsigned char level_count;
	char *addr;
	size_t addr_size;
};

int  blacklist_load(struct blacklist *bl, const char *filename, const char *blacklistname);
int  blacklist_has(struct blacklist *bl, const char *name);
void blacklist_unload(struct blacklist *bl);

/* ========= TIMEGAP ========= */
struct timegap
{
	int start_time; /* seconds since midnight */
	int stop_time; /* seconds since midnight */
	unsigned char week_days; /* bit-map [0:7] (0 <-> 7) */
};

int timegap_init(struct timegap *tg, const char *timer);
int timegap_has(struct timegap *tg, struct tm *time_point);
int timegap_includes(struct timegap *tg, struct timegap *other);

/* ========= TIMEGAPS ========= */
struct timegaps
{
	struct timegap tgap;
	struct list_head list;
};

void timegaps_init(struct timegaps *tgs);
void timegaps_fini(struct timegaps *tgs);
int  timegaps_add(struct timegaps *tgs, struct timegap *tg);
int  timegaps_has(struct timegaps *tgs, struct tm *time_point);

/* ========= BLOCKEE ========= */
#define BLOCKEE_MAX_LEN 16

struct blockee
{
	unsigned char addr[BLOCKEE_MAX_LEN];
};

int blockee_init(struct blockee *be, char *id_str);
int blockee_equal(struct blockee *be, struct blockee *other);
int blockee_is_default(struct blockee *be);

/* ========= BLOCKER ========= */
struct blocker_list
{
	struct timegaps tgaps;
	struct blacklist *bl;
	struct list_head list;
};

struct blocker
{
	struct blockee id; /* default user id in header */
	struct blocker_list bl; /* all blacklists in header */
	struct blocker_list wl; /* white list (exceptions of default blacklist) */
	struct list_head list;
};

void  blocker_init(struct blocker *br);
void  blocker_fini(struct blocker *br);
void  blocker_reinit(struct blocker *br);
int   blocker_load_list(struct blocker *br, const char *filename, const char *listname);
int   blocker_bind_user(struct blocker *br, struct blockee *user, struct timegap *timer, const char *listname, char exclude);
void  blocker_unbind_users(struct blocker *br);
int   blocker_has(struct blocker *br, struct blockee *user, const char *name);

#endif
